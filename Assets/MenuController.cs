﻿using System.Collections;
using Controller;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    #region vars

    public AudioClip ClickSound;
    public AudioClip StartSound;
    public GameObject ExitMuteGameObject;
    public GameObject MainMenuGameObject;
    public GameObject MainMenuBarsGameObject;
    public GameObject SettingsGameObject;
    public GameObject AchievementGameObject;
    public Button ChangeDiffButton;
    public Button MuteAudioButton;
    public TextMeshProUGUI AppVersion;

    [FormerlySerializedAs("LowQualityButton")]
    public Button QualityButton;

    public GameObject CustomSplashScreenGameObject;
    private CustomSplashScreen _customSplashScreen;
    public Button LockControlsButton;
    public GameObject LevelSelectionGameObject;
    public GameObject ExitGameButton;
    public GameObject CloseLevelSelectionButton;
    public GameObject CloseSettingsButton;
    public Button ContinueButton;
    public Button LevelSelectionButton;
    public Button NewGameButton;
    public GameObject StartNewGameDialog;
    public GameObject ChangelogGameObject;

    private const float ActiveButtonColor = 0.9528302f;
    private const float InactiveButtonOpacity = 0.572549f;
    private const float InactiveButtonColor = 0.6886792f;

    private GameManager _gameManager;
    private AudioSource _audioSource;
    private float _animTime = 0.5f;
    private static bool _firstTimeStarted = true;

    #endregion

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        // Lower volume in respect to allowed max volume 
        _audioSource.volume = GlobalGameSettings.MaxAudioVolume / 2f;

        _gameManager = GameManager.Instance;
        if (_firstTimeStarted)
        {
            CustomSplashScreenGameObject.SetActive(true);
            _customSplashScreen = CustomSplashScreenGameObject.GetComponent<CustomSplashScreen>();
            StartCoroutine(_customSplashScreen.StartAnimation());
            _firstTimeStarted = false;
        }
        else
        {
            StartCoroutine(FadeInMainMenu());
        }

        // Update the menu buttons
        if (GlobalGameSettings.GetLastUnlockedLevelNumber() < 2)
        {
            ContinueButton.interactable = false;
            LevelSelectionButton.interactable = false;
            NewGameButton.onClick.AddListener(() => NewGameDialogResponse(true));
        }
        else
        {
            NewGameButton.onClick.AddListener(OnClickNewGame);
        }

        MuteAudioButton.GetComponent<TextMeshProUGUI>().text =
            GlobalGameSettings.AudioMuted() ? "Unmute Music" : "Mute Music";
        QualityButton.GetComponent<TextMeshProUGUI>().text = GlobalGameSettings.GetCurrentQualityString();
        ChangeDiffButton.GetComponent<TextMeshProUGUI>().text =
            "Difficulty: " + GlobalGameSettings.CurrentStoryLoop.ToString();
        ChangeDiffButton.interactable = GlobalGameSettings.CompletedRuns > 0;
        OnClickToggleLockControls(true);

        AppVersion.text = Application.version;
    }

    private IEnumerator IntroScreenAnimation()
    {
        yield return null;
    }

    private IEnumerator FadeInMainMenu()
    {
        StartCoroutine(MainMenuGameObject.GetComponent<CanvasGroup>().FadeCanvasGroupTo(1f, _animTime / 3));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ExitMuteGameObject.GetComponent<CanvasGroup>().FadeCanvasGroupTo(1f, _animTime / 3));
        yield return new WaitForSeconds(_animTime / 3);
        ExitMuteGameObject.GetComponent<CanvasGroup>().alpha = 1f;
        MainMenuGameObject.GetComponent<CanvasGroup>().alpha = 1f;
    }

    public void PlayUiButtonSound(AudioClip clip)
    {
        _audioSource.Stop();
        _audioSource.clip = clip;
        _audioSource.Play();
    }

    public void OnClickMuteSound()
    {
        PlayUiButtonSound(ClickSound);

        // Save settings and update button
        MuteAudioButton.GetComponent<TextMeshProUGUI>().text =
            _gameManager.ToggleMuteAudio() ? "Unmute Music" : "Mute Music";
    }

    public void OnClickToggleLockControls(bool startupCheck = false)
    {
        string[] lockContrStringArray = new[] {"Controls locked", "Controls unlocked"};
        var tmp = LockControlsButton.GetComponent<TextMeshProUGUI>();
        //toggle between locked and unlocked controls
        if (!startupCheck) tmp.text = lockContrStringArray[GlobalGameSettings.ToggleLockedControls()];
        else tmp.text = lockContrStringArray[GlobalGameSettings.GetLockedControls() ? 0 : 1];
    }

    public void OnClickToggleQuality()
    {
        PlayUiButtonSound(ClickSound);

        // Save settings and update button
        GlobalGameSettings.ToggleGraphicsQuality();
        QualityButton.GetComponent<TextMeshProUGUI>().text = GlobalGameSettings.GetCurrentQualityString();
    }

    public void OnClickToggleDifficulty()
    {
        PlayUiButtonSound(ClickSound);

        // Save settings and update button
        GlobalGameSettings.ToggleDifficulty();
        ChangeDiffButton.GetComponent<TextMeshProUGUI>().text =
            "Difficulty: " + GlobalGameSettings.CurrentStoryLoop.ToString();

        FindObjectOfType<PostProcessingCameraChecker>().CheckIfPostProcessingIsNeeded();
    }

    public void OnClickNewGame()
    {
        PlayUiButtonSound(ClickSound);

        // Open newgameDialog
        MainMenuBarsGameObject.SetActive(false);
        StartNewGameDialog.SetActive(true);
    }

    public void NewGameDialogResponse(bool newGame)
    {
        if (newGame)
        {
            PlayUiButtonSound(StartSound);

            //reset values
            GlobalGameSettings.PrepareForNewGame();

            //StartCoroutine(StartGame(StartSound.length, 1)); //old
            //change to intro scene
            SceneManager.LoadScene(GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.IntroScene]);
        }
        else
        {
            PlayUiButtonSound(ClickSound);

            // close newgameDialog
            MainMenuBarsGameObject.SetActive(true);
            StartNewGameDialog.SetActive(false);
        }
    }

    private IEnumerator StartGame(float waitTime, int levelNumber)
    {
        yield return new WaitForSeconds(waitTime);
        //start level from beginning
        _gameManager.LoadScene(levelNumber);
    }

    public void OnClickContinue()
    {
        PlayUiButtonSound(StartSound);

        // Start level with latest level number
        // TODO: Since every level is currently available, this prevents to jump to the ending directly
        //       Replace this with last unlocked after fixed
        StartCoroutine(StartGame(StartSound.length, GlobalGameSettings.GetLastPlayedLevelNumber()));
    }

    public void OnClickCredits()
    {
        SceneManager.LoadScene(GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.EndingScene]);
    }

    public void OnClickCodevemberOrg()
    {
        Application.OpenURL("https://codevember.org/");
    }

    public void OnClickLevelSelection()
    {
        PlayUiButtonSound(ClickSound);

        // Open level selection
        MainMenuGameObject.SetActive(false);
        LevelSelectionGameObject.SetActive(true);
        ExitGameButton.SetActive(false);
        CloseLevelSelectionButton.SetActive(true);
    }

    public void OnClickCloseLevelSelection()
    {
        PlayUiButtonSound(ClickSound);

        // Close level selection
        MainMenuGameObject.SetActive(true);
        LevelSelectionGameObject.SetActive(false);
        ExitGameButton.SetActive(true);
        CloseLevelSelectionButton.SetActive(false);
    }

    public void OnClickOpenSettings()
    {
        PlayUiButtonSound(ClickSound);

        // Open settings
        MainMenuBarsGameObject.SetActive(false);
        SettingsGameObject.SetActive(true);
        ExitGameButton.SetActive(false);
        CloseSettingsButton.SetActive(true);
    }

    public void OnClickCloseSettings()
    {
        PlayUiButtonSound(ClickSound);

        MainMenuBarsGameObject.SetActive(true);
        SettingsGameObject.SetActive(false);
        ExitGameButton.SetActive(true);
        CloseSettingsButton.SetActive(false);
    }

    public void OnClickOpenAchievements()
    {
        PlayUiButtonSound(ClickSound);
        MainMenuBarsGameObject.SetActive(false);
        AchievementGameObject.SetActive(true);
    }

    public void OnClickCloseAchievements()
    {
        PlayUiButtonSound(ClickSound);
        MainMenuBarsGameObject.SetActive(true);
        AchievementGameObject.SetActive(false);
    }

    public void OnClickExitGame()
    {
        PlayUiButtonSound(ClickSound);
        Application.Quit();
    }

    public void OnClickIntroScreen()
    {
        PlayUiButtonSound(ClickSound);
        StartCoroutine(FadeOutIntroScreen());
    }

    private IEnumerator FadeOutIntroScreen()
    {
        yield return StartCoroutine(CustomSplashScreenGameObject.GetComponent<CanvasGroup>()
            .FadeCanvasGroupTo(0f, _animTime / 2));
        CustomSplashScreenGameObject.SetActive(false);
        //wait
        yield return new WaitForSeconds(_animTime);
        //interrupt here if changelog has never been seen bevor
        if (!GlobalGameSettings.ChangelogSeen)
        {
            //display changelog
            ChangelogGameObject.SetActive(true);
        }
        else StartCoroutine(ContinueFadeOutIntroScreen());
    }

    public void OnClickChangelogConfirm()
    {
        GlobalGameSettings.ChangelogSeen = true;
        ChangelogGameObject.SetActive(false);
        StartCoroutine(ContinueFadeOutIntroScreen());
    }

    private IEnumerator ContinueFadeOutIntroScreen()
    {
        //enjoy level design
        yield return new WaitForSeconds(_animTime * 2);
        //fade in menu and exit/mute btns
        yield return StartCoroutine(FadeInMainMenu());
    }
}