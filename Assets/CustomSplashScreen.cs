﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CustomSplashScreen : MonoBehaviour
{
    public GameObject IntroBtnGameObject;
    public GameObject CvLogo;
    public GameObject UnderwatchLogo;

    private CanvasGroup _cg;
    private Button _introBtn;

    public IEnumerator StartAnimation()
    {
        _introBtn = GetComponent<Button>();
        _cg = GetComponent<CanvasGroup>();
        _introBtn.interactable = false;
        _cg.alpha = 0;
        yield return new WaitForSeconds(0.5f);
        //fade in cv logo
        CvLogo.SetActive(true);
        StartCoroutine(_cg.FadeCanvasGroupTo(1f, 1f));
        yield return new WaitForSeconds(2f);
        //fade out cv logo
        StartCoroutine(_cg.FadeCanvasGroupTo(0f, 1f));
        yield return new WaitForSeconds(1f);
        CvLogo.SetActive(false);
        UnderwatchLogo.SetActive(true);
        //fade in underwatch logo and button to continue
        //yield return new WaitForSeconds(0.5f);
        StartCoroutine(_cg.FadeCanvasGroupTo(1f, 1f));
        yield return new WaitForSeconds(1f);
        IntroBtnGameObject.SetActive(true);
        _introBtn.interactable = true;
    }
}
