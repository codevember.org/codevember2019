﻿using System;
using System.Collections;
using System.Globalization;
using Controller;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class PostProcessingCameraChecker : MonoBehaviour
{
    public GameObject PostProcessingVolume;
    public GameObject FogSmallRoom;
    public GameObject FogBigRoom;

    private PostProcessVolume _ppVolume;
    private Bloom _bloomLayer = null;
    private ColorGrading _colorGradingLayer = null;
    private LensDistortion _lensDistortionLayer = null;
    private Vignette _vignetteLayer = null;
    private ChromaticAberration _chromaticAberration = null;
    private bool _oneTime = true;
    private bool _activateDistractions = false;

    private void Start()
    {
        // Always check except the transition or intro scene
        if (SceneManager.GetActiveScene() ==
            SceneManager.GetSceneByName(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.IntroScene]) ||
            SceneManager.GetActiveScene() ==
            SceneManager.GetSceneByName(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.TransitionScene]))
        {
        }
        else 
        {
            CheckIfPostProcessingIsNeeded();
        }
    }

    public void CheckIfPostProcessingIsNeeded()
    {
        _activateDistractions = GlobalGameSettings.CurrentStoryLoop == GlobalGameSettings.StoryLoop.Hard;
        _ppVolume = PostProcessingVolume.GetComponent<PostProcessVolume>();
        if (_activateDistractions)
        {
            // If needed, set it
            // Get vars
            _ppVolume.profile.TryGetSettings(out _bloomLayer);
            _ppVolume.profile.TryGetSettings(out _colorGradingLayer);
            _ppVolume.profile.TryGetSettings(out _lensDistortionLayer);
            _ppVolume.profile.TryGetSettings(out _vignetteLayer);
            _ppVolume.profile.TryGetSettings(out _chromaticAberration);

            // Bloom - intensity from 0 to 12
            _bloomLayer.intensity.value = GlobalGameSettings.BloomIntensity;
            // Cc - contrast from 0 to 10
            _colorGradingLayer.contrast.value = GlobalGameSettings.CcContrast;
            // Cc - saturation from 0 to 50
            _colorGradingLayer.saturation.value = GlobalGameSettings.CcSaturation;
            // Lens distortion - intensity from 0 to 40
            _lensDistortionLayer.intensity.value = GlobalGameSettings.LensDistortionIntensity;
            // Vignetting - intensity from 0 - 0.36
            _vignetteLayer.intensity.value = GlobalGameSettings.VignetteIntensity;
        }

        PostProcessingVolume.SetActive(_activateDistractions);
        PostProcessingVolume.transform.parent.gameObject.GetComponent<PostProcessLayer>().enabled =
            _activateDistractions;

        // Set fog only if in main menu
        if (SceneManager.GetActiveScene() ==
            SceneManager.GetSceneByName(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.MainMenuScene]))
        {
            SetFog(GameObject.FindGameObjectWithTag("Level").transform.GetChild(0).gameObject);
        }
    }

    void Update()
    {
        if (!_activateDistractions) return;
        if (_oneTime && GlobalGameSettings.LevelGameObject != null)
        {
            _oneTime = false;
            // Find out which room size, then activate it fog if needed
            var lvl = GlobalGameSettings.LevelGameObject.transform.GetChild(0).gameObject;
            SetFog(lvl);
            //Debug.Log("call from here");
        }
    }

    private void SetFog(GameObject lvlGo)
    {
        if (lvlGo.name.IndexOf("large", StringComparison.CurrentCultureIgnoreCase) >= 0)
        {
            // Activate fog for big rooms
            FogBigRoom.SetActive(_activateDistractions);
        }
        else
            FogSmallRoom.SetActive(_activateDistractions);
    }

    public void TriggerChromaticAberrationImpulse()
    {
        if (GlobalGameSettings.CurrentStoryLoop == GlobalGameSettings.StoryLoop.Hard)
        {
            StartCoroutine(ChromaticAberrationInstantImpulse(6, 1f));
        }
        else
        {
            Debug.Log("Error: Postprocessing stack is disabled." +
                      "Please don't use postprocessing effects if not in hard mode");
        }
    }

    // Chromatic aberration impulse starting with 0, fading to peakValue and back to 0
    private IEnumerator ChromaticAberrationSinusImpulse(double peakValue, float duration)
    {
        const int steps = 20;

        for (var i = 0; i <= steps; i++)
        {
            var currentMultiplier = Math.Sin(Math.PI / steps * i);
            var currentValue = (float) (currentMultiplier * peakValue);
            _chromaticAberration.intensity.value = currentValue;
            yield return new WaitForSeconds(duration / steps);
        }
    }

    // Chromatic aberration impulse starting with peakValue and fading back to 0
    private IEnumerator ChromaticAberrationInstantImpulse(double peakValue, float duration)
    {
        const int steps = 20;
        const double halfOfPi = Math.PI / 2;

        for (var i = 0; i <= steps; i++)
        {
            var currentMultiplier = Math.Cos(halfOfPi / steps * i);
            var currentValue = (float) (currentMultiplier * peakValue);
            _chromaticAberration.intensity.value = currentValue;
            yield return new WaitForSeconds(duration / steps);
        }
    }
}