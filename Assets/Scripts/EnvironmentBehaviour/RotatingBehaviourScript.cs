﻿using UnityEngine;

namespace EnvironmentBehaviour
{
    public class RotatingBehaviourScript : MonoBehaviour
    {
        [Tooltip("Degrees per second")] public float speed = 1f;
        [Tooltip("Inverse direction")] public bool inverse = false;

        [Tooltip("Enable seesaw instead of complete rotation")]
        public bool seesaw = false;

        [Tooltip("Seesaw range in degrees")] public float seesawDegreeRange = 360f;
        [Tooltip("Phase offset")] public float phaseShifter = 0f;
        [Tooltip("Enable of axis")] public bool xAxis, yAxis, zAxis;

        [Space(10)]
        public bool localRotation = false;

        private float pi = 3.1415f;
        private Vector3 _initialRotation;
        private float _startTime = 0f;

        private void Start()
        {
            _initialRotation = gameObject.transform.rotation.eulerAngles; // Persist initial setup
            if(localRotation) _initialRotation = gameObject.transform.localRotation.eulerAngles;
            _startTime = Time.fixedTime;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            var dirSpeed = (inverse) ? speed * -1f : speed;
            var time = Time.fixedTime - _startTime;

            if (seesaw)
            {
                var rotation = Mathf.Sin(time * dirSpeed + phaseShifter * (pi / 2)) * seesawDegreeRange;
                if (localRotation)
                {
                    gameObject.transform.localRotation = Quaternion.Euler(_initialRotation.x + (xAxis ? rotation : 0),
                        _initialRotation.y + (yAxis ? rotation : 0),
                        _initialRotation.z + (zAxis ? rotation : 0));
                }
                else
                {
                    gameObject.transform.rotation = Quaternion.Euler(_initialRotation.x + (xAxis ? rotation : 0),
                        _initialRotation.y + (yAxis ? rotation : 0),
                        _initialRotation.z + (zAxis ? rotation : 0));
                }
            }
            else
            {
                var rotation = time * dirSpeed;
                if (localRotation)
                {
                    gameObject.transform.localRotation = Quaternion.Euler(_initialRotation.x + (xAxis ? rotation : 0),
                        _initialRotation.y + (yAxis ? rotation : 0),
                        _initialRotation.z + (zAxis ? rotation : 0));
                }
                else
                {
                    gameObject.transform.rotation = Quaternion.Euler(_initialRotation.x + (xAxis ? rotation : 0),
                        _initialRotation.y + (yAxis ? rotation : 0),
                        _initialRotation.z + (zAxis ? rotation : 0));
                }
            }
        }
    }
}