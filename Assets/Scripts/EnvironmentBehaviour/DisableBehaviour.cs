﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Controller;
using UnityEngine;

namespace EnvironmentBehaviour
{
    public class DisableBehaviour : MonoBehaviour
    {
        public float secondsToWait = 0;

        [Tooltip("Disables itself only when low graphics mode is active, also ignores secondsToWait")]
        public bool disableOnLowGraphicsQualityOnly = false;

        public bool showAndWait;

        // Start is called before the first frame update
        private void Start()
        {
            if (disableOnLowGraphicsQualityOnly || showAndWait)
            {
                StartCoroutine(WaitAndDeactivate());
            }
            else
            {
                StartCoroutine(WaitAndActivate(GetComponentsInChildren<MeshRenderer>().ToList(),
                    GetComponentsInChildren<BoxCollider>().ToList()));
            }
        }

        // Disables a game object after a given interval
        private IEnumerator WaitAndActivate(List<MeshRenderer> meshes, List<BoxCollider> boxColliders)
        {
            meshes.ForEach(x => x.enabled = false);
            boxColliders.ForEach(x => x.enabled = false);
            yield return new WaitForSeconds(0.02f);

            var lowGraphicsQualityModeEnabled = (Application.platform == RuntimePlatform.Android)
                ? GlobalGameSettings.GetCurrentQuality() != GlobalGameSettings.Quality.High
                : GlobalGameSettings.GetCurrentQuality() == GlobalGameSettings.Quality.Low;
            if (lowGraphicsQualityModeEnabled || !showAndWait)
            {
                yield return new WaitForSeconds(secondsToWait);
            }

            if (!disableOnLowGraphicsQualityOnly || lowGraphicsQualityModeEnabled || !showAndWait)
            {
                meshes.ForEach(x => x.enabled = true);
                boxColliders.ForEach(x => x.enabled = true);
            }
        }

        // Disables a game object after a given interval
        private IEnumerator WaitAndDeactivate()
        {
            yield return new WaitForSeconds(0.01f);

            var lowGraphicsQualityModeEnabled =
                GlobalGameSettings.GetCurrentQuality() == GlobalGameSettings.Quality.Low;
            if (lowGraphicsQualityModeEnabled || showAndWait)
            {
                yield return new WaitForSeconds(secondsToWait);
            }

            if (!disableOnLowGraphicsQualityOnly || lowGraphicsQualityModeEnabled || showAndWait)
            {
                gameObject.SetActive(false);
            }
        }
    }
}