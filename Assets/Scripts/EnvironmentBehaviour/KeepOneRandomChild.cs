﻿using Controller;
using UnityEngine;

namespace EnvironmentBehaviour
{
    public class KeepOneRandomChild : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            // Keep same track for each level, don't choose random
            // Alternative: var randomPick = Random.Range(0, transform.childCount);
            var currentLevelNumber = GameManager.Instance.GetCurrentLevelNumber();
            var currentTrack = currentLevelNumber % transform.childCount;
            //Debug.Log("lvl no: " + GameManager.Instance.GetCurrentLevelNumber() + ", count: " + transform.childCount + "chosen:" + currentTrack);

            for (var i = 0; i < transform.childCount; i++)
            {
                if (i == currentTrack)
                {
                    var child = transform.GetChild(i);
                    // Should be already active: child.gameObject.SetActive(true);
                    // Pitch higher, the higher we get
                    child.GetComponent<AudioSource>().pitch = 0.3f + (currentLevelNumber / 80f) * 0.6f;
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }
}