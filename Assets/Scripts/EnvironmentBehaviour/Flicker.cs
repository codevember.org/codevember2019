﻿using System.Collections;
using UnityEngine;

namespace EnvironmentBehaviour
{
    public class Flicker : MonoBehaviour
    {
        [Range(0f, 1f)] public float threshold;

        public bool superFast;

        // Specifies the camera to flicker
        public GameObject whatToFlicker;

        // Specifies the warning lights which are disabled at first and start 0.5s before the cam starts
        public GameObject[] warningLights;

        private int _frames = 0;

        private void Start()
        {
            foreach (var warningLight in warningLights)
            {
                warningLight.SetActive(false);
            }
        }

        private void Update()
        {
            _frames++;
            var ft = superFast ? 8 : 60;
            if (_frames % ft != 0) return;

            var rndVal = Random.Range(0f, 1f);
            if (!(rndVal >= threshold)) return;

            // Toggle active state
            foreach (var warningLight in warningLights)
            {
                if (!whatToFlicker.activeSelf)
                {
                    warningLight.SetActive(true);
                }
            }

            StartCoroutine(WaitAndToggle());
        }

        private IEnumerator WaitAndToggle()
        {
            yield return new WaitForSeconds(0.8f);
            whatToFlicker.SetActive(!whatToFlicker.activeSelf);

            foreach (var warningLight in warningLights)
            {
                warningLight.SetActive(false);
            }
        }
    }
}