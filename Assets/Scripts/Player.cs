﻿using System.Collections;
using System.Collections.Generic;
using Controller;
using UnityEngine;
using UnityEngine.Playables;

public class Player : MonoBehaviour
{
    public List<AudioClip> alarmSoundFxs = new List<AudioClip>();
    public List<AudioClip> moveSoundFxs = new List<AudioClip>();
    public List<AudioClip> dieSoundFxs = new List<AudioClip>();
    public List<AudioClip> woundedSoundFxs = new List<AudioClip>();
    public List<AudioClip> distortionSoundFxs = new List<AudioClip>();
    
    public AudioSource soundFxSource;
    public AudioSource distractionFxSource;

    [HideInInspector] public static bool IsMoving;

    private Animator _ratAnimator;
    private bool _oneTime = true;
    private bool _once = true;
    
    private void Start()
    {
        _ratAnimator = GetComponent<Animator>();
        soundFxSource.volume = GlobalGameSettings.MaxAudioVolume / 2;
    }

    public void Die()
    {
        StartCoroutine(PlayDyingSounds());
    }

    private IEnumerator PlayDyingSounds()
    {
        // Alarm
        distractionFxSource.Stop();
        distractionFxSource.loop = false;
        distractionFxSource.clip = alarmSoundFxs[Random.Range(0, alarmSoundFxs.Count)];
        distractionFxSource.pitch = 1f;
        distractionFxSource.volume = 0.2f;
        distractionFxSource.Play();
        yield return new WaitForSeconds(0.4f);

        // He ded, do dem sound
        soundFxSource.Stop();
        soundFxSource.loop = false;
        soundFxSource.pitch = 0.8f;
        soundFxSource.clip = dieSoundFxs[Random.Range(0, dieSoundFxs.Count)];
        soundFxSource.Play();
    }

    public void ReachedFinish()
    {
        soundFxSource.Stop();
    }

    public void GetWounded()
    {
        // He dead? naah bruh
        soundFxSource.Stop();
        soundFxSource.loop = false;
        soundFxSource.pitch = 1f;
        soundFxSource.clip = woundedSoundFxs[Random.Range(0, woundedSoundFxs.Count)];
        soundFxSource.Play();
    }
    
    public void DistortionSoundEffect()
    {
        // Skrrr skrrr skrrrr
        distractionFxSource.Stop();
        distractionFxSource.loop = false;
        distractionFxSource.clip = distortionSoundFxs[Random.Range(0, distortionSoundFxs.Count)];
        distractionFxSource.pitch = 0.4f;
        distractionFxSource.volume = 1.0f;
        distractionFxSource.Play();
    }

    private void Update()
    {
        if (IsMoving)
        {
            if (!_oneTime) return;

            _ratAnimator.StopPlayback();
            // Set move sound
            soundFxSource.Stop();
            soundFxSource.loop = true;
            soundFxSource.pitch = 1f;
            soundFxSource.clip = moveSoundFxs[Random.Range(0, moveSoundFxs.Count)];
            soundFxSource.Play();
            _oneTime = false;
            _once = false;
        }
        else
        {
            _ratAnimator.StartPlayback();
            if (!_once)
            {
                // Stop sound
                soundFxSource.Stop();
                soundFxSource.loop = false;
                _once = true;
            }
            _oneTime = true;
        }
    }
}