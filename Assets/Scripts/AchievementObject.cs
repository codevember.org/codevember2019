﻿using System.Collections;
using System.Collections.Generic;
using Controller;
using TMPro;
using UnityEngine;

public class AchievementObject : MonoBehaviour
{
    public TextMeshProUGUI tmpFlag;
    public TextMeshProUGUI tmpTitle;
    public TextMeshProUGUI tmpContent;

    private bool _isDone
    {
        set
        {
            if (value)
            {
                tmpFlag.text = "O";
                GetComponent<CanvasGroup>().alpha = 1f;
            }
            else
            {
                tmpFlag.text = "";
                GetComponent<CanvasGroup>().alpha = 0.5f;
            }
        }
    }

    public void SetupAchievement(GlobalGameSettings.Achievement achievement)
    {
        tmpTitle.text = GlobalGameSettings.GetAchievementTitle(achievement);
        tmpContent.text = GlobalGameSettings.GetAchievementDescription(achievement);
        _isDone = GlobalGameSettings.IsAchievementDone(achievement);
    }
}