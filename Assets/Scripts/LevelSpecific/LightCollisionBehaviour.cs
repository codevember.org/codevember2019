﻿using Controller;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LevelSpecific
{
    public class LightCollisionBehaviour : MonoBehaviour
    {
        // The degrees which are subtracted of the spotlight angle (higher value = less strict)
        private const int SpotlightAngleThreshold = 2;

        // Flag showing if the debug mode is enabled
        private bool _hitDebugMode;

        private readonly List<Light> _spotLights = new List<Light>();

        private GameManager _gameManager;
        private Image _overlayImage;
        private Color _currentOverlayColor;
        private int _softThreshold = GlobalGameSettings.InitialLightCollisionThreshold;

        private void Awake()
        {
            _gameManager = GameManager.Instance;
        }

        // Start is called before the first frame update
        private void Start()
        {
            _hitDebugMode = Debug.isDebugBuild;

            // Debug.Log("Checking for light hits...");
            _overlayImage = GameObject.FindGameObjectWithTag("Overlay").GetComponent<Image>();
            _currentOverlayColor = _overlayImage.color;
        }

        public void UpdateSurveillanceCameraLights()
        {
            _spotLights.Clear();

            foreach (var spotLightGameObject in GameObject.FindGameObjectsWithTag("CameraLight"))
            {
                _spotLights.Add(spotLightGameObject.GetComponent<Light>());
            }
        }

        // Update is called once per frame
        public void FixedUpdate()
        {
            if (!_gameManager.checkLightCollision || _gameManager.GetActiveUiState() != GameManager.UiState.Playing ||
                _spotLights.Count == 0) return;

            // Check if there is collision
            var hitLights = new List<Tuple<Vector3, Vector3>>();
            foreach (var spotLight in _spotLights)
            {
                if (!spotLight.isActiveAndEnabled) continue;

                var spotLightPosition = spotLight.transform.position;
                var playerPosition = transform.position;
                var spotLightToPlayerDirection = playerPosition - spotLightPosition;

                var angleToSpotlightCenter = Vector3.Angle(spotLightToPlayerDirection, spotLight.transform.forward);
                var spotlightAngle = spotLight.spotAngle / 2;

                // Cast ray from light to player
                if (Physics.Raycast(spotLight.transform.position, spotLightToPlayerDirection, out var hit) &&
                    hit.collider.CompareTag("PlayerBody") &&
                    Math.Abs(angleToSpotlightCenter) < spotlightAngle - SpotlightAngleThreshold)
                {
                    hitLights.Add(new Tuple<Vector3, Vector3>(spotLightPosition, spotLightToPlayerDirection));
                    spotLight.color = Color.Lerp(spotLight.color, Color.red, 0.08f);
                }
                else if (_hitDebugMode)
                {
                    Debug.DrawRay(spotLight.transform.position, spotLightToPlayerDirection, Color.green, 5.0f, false);
                }
            }

            // If there is a hit, don't reset the state - otherwise do
            if (hitLights.Count > 0)
            {
                foreach (var (position, direction) in hitLights)
                {
                    HitLight(position, direction);
                }
            }
            else
            {
                ResetState();
            }
        }

        private void ResetState()
        {
            _currentOverlayColor = _currentOverlayColor * new Color(1, 1, 1, 0);
            _overlayImage.color = _currentOverlayColor;
            _softThreshold = GlobalGameSettings.InitialLightCollisionThreshold;

            foreach (var spotLight in _spotLights)
            {
                spotLight.color = Color.white;
            }
        }

        private void HitLight(Vector3 position, Vector3 direction)
        {
            _softThreshold--;
            _currentOverlayColor = _currentOverlayColor +
                                   new Color(0, 0, 0, (1f / GlobalGameSettings.InitialLightCollisionThreshold));
            _overlayImage.color = _currentOverlayColor;
            if (_softThreshold == 0)
            {
                _overlayImage.gameObject.SetActive(false);
                _gameManager.PlayerDied();
                return;
            }

            if (_hitDebugMode)
            {
                Debug.DrawRay(position, direction, Color.red, 5.0f, false);
            }
        }
    }
}