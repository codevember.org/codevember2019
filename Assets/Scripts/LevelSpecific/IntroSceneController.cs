﻿using System.Collections;
using System.Collections.Generic;
using Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelSpecific
{
    public class IntroSceneController : MonoBehaviour
    {
        public AudioSource RatNoise;
        public CanvasGroup BlackOverlay;
        public CanvasGroup SeizureTextBox;
        public TextMeshProUGUI SeizureText;
        public CanvasGroup ThoughtBubbleBox;
        public TextMeshProUGUI ThoughtBubbleText;
        public Button SkipButton;
        public float TimePerText = 4f;

        [TextArea(2, 4)] public List<string> IntroText = new List<string>();
        [TextArea(7, 12)] public string SeizureWarningText;

        void Start()
        {
            if (GameManager.Initialized) GameManager.Instance.GetComponent<AudioSource>().Stop();
            SeizureTextBox.alpha = 0f;
            SeizureText.text = SeizureWarningText;
            ThoughtBubbleBox.alpha = 0f;
            BlackOverlay.alpha = 1f;
            StartCoroutine(StartIntroScene());
            SkipButton.gameObject.SetActive(GlobalGameSettings.NotFirstTimePlaying);
        }

        private IEnumerator StartIntroScene()
        {
            var audioSource = GetComponent<AudioSource>();
            yield return new WaitForSeconds(1f);
            // Fade in seizure text
            StartCoroutine(SeizureTextBox.FadeCanvasGroupTo(1f, 1f));
            //fade out text after some time
            yield return new WaitForSeconds(5f);
            StartCoroutine(SeizureTextBox.FadeCanvasGroupTo(0f, 1f));
            // Start camera movement
            yield return new WaitForSeconds(1f);
            SeizureTextBox.gameObject.SetActive(false);
            audioSource.Play();
            yield return new WaitForSeconds(1f);
            StartCoroutine(Extension.MoveCamera(IntroText.Count * TimePerText + 7f, -22.5f));
            // Fade out black
            StartCoroutine(BlackOverlay.FadeCanvasGroupTo(0f, 2f));
            yield return new WaitForSeconds(4f);
            StartCoroutine(ThoughtBubbleBox.FadeCanvasGroupTo(1f, 1f));
            yield return new WaitForSeconds(1f);

            // Show intro text in overlay
            for (var i = 0; i < IntroText.Count; i++)
            {
                RatNoise.pitch = Random.Range(0.9f, 1.1f);
                if ((i % 2) == 0) RatNoise.Play();
                var text = IntroText[i];
                ThoughtBubbleText.text = text;
                yield return new WaitForSeconds(TimePerText);
            }

            // Fade out audio
            StartCoroutine(audioSource.FadeAudioSource(0f, 1.5f));
            StartCoroutine(ThoughtBubbleBox.FadeCanvasGroupTo(0f, 1f));
            // Fade in black
            StartCoroutine(BlackOverlay.FadeCanvasGroupTo(1f, 1f));
            yield return new WaitForSeconds(2f);
            GlobalGameSettings.NotFirstTimePlaying = true;
            // Change to game scene
            SwitchToGameSceneAndStartGame();
        }

        void SwitchToGameSceneAndStartGame()
        {
            // Start level with latest level number
            GameManager.Instance.LoadScene(1);
        }

        public void OnClickSkip()
        {
            StopCoroutine(StartIntroScene());
            SwitchToGameSceneAndStartGame();
        }
    }
}