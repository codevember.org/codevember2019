﻿using System;
using System.Linq;
using Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelSpecific.ChapterSelection
{
    public class ChapterController : MonoBehaviourSingleton<ChapterController>
    {
        public TextMeshProUGUI chapterNameTmp;
        public ChapterTab defaultChapterTab;
        public Color32 highlightedTabColor;
        public Color32 defaultTabColor;
        public Sprite lockedLevelThumbnail;

        private ChapterTab _activeTab;
        private MenuController _menuController;

        protected override void Start()
        {
            base.Start();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _activeTab = defaultChapterTab;

            var tabs = defaultChapterTab.transform.parent.GetComponentsInChildren<ChapterTab>().ToList();

            foreach (var tab in tabs)
            {
                if (tab == _activeTab)
                {
                    _activeTab.AssociatedLevelsGameObject.SetActive(true);
                    continue;
                }
                tab.AssociatedLevelsGameObject.SetActive(false);
                tab.GetComponent<Image>().color = defaultTabColor;
            }


            _activeTab.GetComponent<Image>().color = highlightedTabColor;

            if (_menuController == null) _menuController = FindObjectOfType<MenuController>();
        }

        public void TabGotClicked(ChapterTab clickedTab)
        {
            _menuController.PlayUiButtonSound(_menuController.ClickSound);
            if (_activeTab == clickedTab) return;

            // Deselect old _activeTab...
            _activeTab.AssociatedLevelsGameObject.SetActive(false);
            clickedTab.AssociatedLevelsGameObject.SetActive(true);
            // ... revert color
            _activeTab.GetComponent<Image>().color = defaultTabColor;
            // ... highlight this tab
            clickedTab.GetComponent<Image>().color = highlightedTabColor;

            _activeTab = clickedTab;

            // ... and finally change text
            chapterNameTmp.text = clickedTab.ChapterName;
        }

        public void OnClickLevelSelected(int levelNumber)
        {
            _menuController.PlayUiButtonSound(_menuController.StartSound);
            StartSelectedLevel(levelNumber);
        }

        /** Returns whether a level is already accessible by the user. */
        public void StartSelectedLevel(int levelNumber)
        {
            _menuController.PlayUiButtonSound(_menuController.StartSound);
            GameManager.Instance.LoadScene(levelNumber);
        }
    }
}