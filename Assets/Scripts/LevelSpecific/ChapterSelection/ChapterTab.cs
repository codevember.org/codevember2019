﻿using LevelSpecific;
using LevelSpecific.ChapterSelection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChapterTab : MonoBehaviour
{
    public string ThumbnailText;
    public string ChapterName;
    public GameObject AssociatedLevelsGameObject;

    void Start()
    {
        var shortName = GetComponentInChildren<TextMeshProUGUI>().text;
        if (!shortName.Equals(ThumbnailText))
        {
            GetComponentInChildren<TextMeshProUGUI>().text = ThumbnailText;
            Debug.Log("kommt rein##");
        }
        ChapterName = "<u>" + ChapterName + "</u>";

        //add btn click
        GetComponent<Button>().onClick.AddListener(OnChapterTapClick);
    }

    private void OnChapterTapClick()
    {
        ChapterController.Instance.TabGotClicked(this);
    }
}