﻿using System.Collections;
using Controller;
using UnityEngine;

namespace LevelSpecific
{
    public class MovePlayerToWallMonitor : MonoBehaviour
    {
        private Player _player;
        private Outline _playerOutline;

        private void Start()
        {
            // Only start animation if this is the first time the user plays this level
            if (GlobalGameSettings.GetLastPlayedLevelIteration() == 0)
            {
                _player = GameManager.Instance.GetCurrentPlayer().GetComponent<Player>();
                _playerOutline = _player.GetComponent<Outline>();
                StartCoroutine(MovePlayer());
            }
        }

        private IEnumerator MovePlayer()
        {
            _playerOutline.enabled = false;

            var time = 1.5f;
            var startPos = _player.transform.position;
            var targetPos = new Vector3(-2.97f, startPos.y, -0.0146f);
            float elapsedTime = 0;

            yield return new WaitForSeconds(time / 3);
            while (elapsedTime < time * 1)
            {
                //_player.transform.position = Vector3.Lerp(startPos, targetPos, (elapsedTime / time));
                GameManager.Instance.MovePlayer(Mathf.Lerp(0f, 0.025f, elapsedTime), 0);
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            elapsedTime = 0;

            while (elapsedTime < time)
            {
                //_player.transform.position = Vector3.Lerp(startPos, targetPos, (elapsedTime / time));
                GameManager.Instance.MovePlayer(0, 0.018f);
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            var joystick = FindObjectOfType<JoystickInputController>();
            while (!joystick.MovementEnableCheck())
            {
                yield return null;
            }

            _playerOutline.enabled = true;
        }
    }
}