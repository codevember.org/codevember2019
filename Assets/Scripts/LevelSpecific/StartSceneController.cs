﻿using System.Collections;
using Controller;
using UnityEngine;

namespace LevelSpecific
{
    public class StartSceneController : MonoBehaviour
    {
        public CanvasGroup blackOverlay;
        public GameObject joystickGameObject;

        private AudioSource _audioSource;
        private Player _player;
        private Animator _cameraAnimator;
        private Animator _joystickAnimator;
        private JoystickInputController _joystickInputController;
        private bool _correctUserInput = false;

        // Start is called before the first frame update
        private void Start()
        {
            if (GlobalGameSettings.CompletedRuns == 0)
            {
                blackOverlay.alpha = 1f;
                _cameraAnimator = Camera.main.GetComponent<Animator>();
                _cameraAnimator.enabled = true;
                _joystickAnimator = GetComponent<Animator>();
                _audioSource = gameObject.GetComponent<AudioSource>();
                _player = GameManager.Instance.GetCurrentPlayer().GetComponent<Player>();
                _player.GetComponent<Outline>().enabled = false;
                StartCoroutine(GameStartAnimation());
            }
        }

        private IEnumerator GameStartAnimation()
        {
            //deactivate input
            GameManager.Instance.EnablePlayerMovement(false);
            //start animation; show outline after 6 secs
            _cameraAnimator.Play("CameraMovementStartScene");
            yield return new WaitForSeconds(0.3f);
            //fade out black
            StartCoroutine(blackOverlay.FadeCanvasGroupTo(0f, 2f));
            yield return new WaitForSeconds(2f);
            blackOverlay.gameObject.SetActive(false);
            yield return new WaitForSeconds(3f);
            _player.GetComponent<Outline>().enabled = true;
            //mouse squeaking
            _audioSource.volume = GlobalGameSettings.MaxAudioVolume;
            _audioSource.Play();
            yield return new WaitForSeconds(7f);

            //show how to controle input
            //activate input, wait for user input
            //_animator.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
            _joystickInputController = FindObjectOfType<JoystickInputController>();
            GameManager.Instance.EnablePlayerMovement(true);
            //ui animation
            joystickGameObject.SetActive(true);
            _joystickAnimator.Play("JoystickTutorial");
            while (!_correctUserInput)
            {
                //check for correct user input
                if (_joystickInputController.TouchStart)
                {
                    //disable animation when touch started
                    _joystickAnimator.enabled = false;
                    joystickGameObject.SetActive(false);

                    if (_joystickInputController.ReachedMaxVelocity)
                    {
                        _correctUserInput = true;
                    }
                }
                else
                {
                    //activate knob animation again if user didnt reach max velocity of mouse
                    joystickGameObject.SetActive(true);
                    _joystickAnimator.enabled = true;
                    _joystickAnimator.Play("JoystickTutorial");
                }

                yield return null;
            }

            yield return new WaitForSeconds(1f);
            _cameraAnimator.enabled = false;

            yield return null;
        }
    }
}