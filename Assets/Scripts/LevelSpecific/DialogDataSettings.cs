using System;
using System.Collections.Generic;

namespace LevelSpecific
{
    public static class DialogDataSettings
    {
        public static readonly Dictionary<string, string[]> Dialogs = new Dictionary<string, string[]>
        {
            {
                "000_intro", new[]
                {
                    "Is this working yet?",
                    "...oh"
                }
            },
            {
                "010_intro", new[]
                {
                    "I've been watching you for quite a while.",
                    "Finally, I managed to open up your cage.",
                    "You can move by pressing anywhere on the screen...",
                    "Try to reach the exit door!"
                }
            },
            {
                "011_sorry_i_am_chuck", new[]
                {
                    "Sorry, I'm Chuck by the way!",
                    "I'll help you escape this lab.",
                    "Avoid the surveillance camera lights...",
                    "they will detect you!"
                }
            },
            {
                "014_it_is_okay_to_pass", new[]
                {
                    "Sometimes you need to pass the light...",
                    "... you can, if you're fast enough!"
                }
            },
            {
                "015_good", new[]
                {
                    "Good..."
                }
            },
            {
                "015_luck", new[]
                {
                    "...luck!"
                }
            },
            {
                "016_doing_great", new[]
                {
                    "You're doing great!"
                }
            },
            {
                "023_me_again", new[]
                {
                    "It's me again...",
                    "...just wanted to check if you're still on track"
                }
            },
            {
                "026_grandfather", new[]
                {
                    "Did you know...",
                    "that my grandfather worked in this lab?"
                }
            },
            {
                "030_more_technical", new[]
                {
                    "It will get more demanding..."
                }
            },
            {
                "031_mainframe", new[]
                {
                    "Maybe you will also find the mainframe!"
                }
            },
            {
                "032_messy", new[]
                {
                    "Sometimes things seem messy...",
                    "...just wait for the right moment!"
                }
            },
            {
                "033_patient_and_hide", new[]
                {
                    "Be patient...",
                    "and hide if you need to"
                }
            },
            {
                "035_real_shadow", new[]
                {
                    "Remember: The only real shadow...",
                    "...is the darkest one!"
                }
            },
            {
                "036_more_intense", new[]
                {
                    "Things will get more intense...",
                    "...stay calm!"
                }
            },
            {
                "040_slogans", new[]
                {
                    "And don't confuse me with those motivational slogans..."
                }
            },
            {
                "041_be_patient", new[]
                {
                    "Sometimes...",
                    "...you just need...",
                    "...to be...",
                    "...patient!"
                }
            },
            {
                "042_do_not_be_afraid", new[]
                {
                    "don't be afraid of stuff...",
                    "that doesn't kill you"
                }
            },
            {
                "043_do_not", new[]
                {
                    "don't"
                }
            },
            {
                "044_never_forget", new[]
                {
                    "Never forget what you really are...",
                    "...and what you are able to do!"
                }
            },
            {
                "045_save_space", new[]
                {
                    "< safe",
                    "space"
                }
            },
            {
                "051_it_is_okay_to_hide", new[]
                {
                    "It's also okay...",
                    "to hide sometimes"
                }
            },
            {
                "053_quite_some_action", new[]
                {
                    "Wow... there was quite some action going on...",
                    "No one is watching you right now...",
                    "so relax for a moment!"
                }
            },
            {
                "054_step_by_step", new[]
                {
                    "Step by step!",
                    "Keep waiting for the right moment!",
                    "And take your chance!"
                }
            },
            {
                "061_closer", new[]
                {
                    "You're getting closer..."
                }
            },
            {
                "063_dark_side", new[]
                {
                    "Stay on the dark side!",
                    "It's the only option!"
                }
            },
            {
                "110_harder", new[]
                {
                    "This may seem alike...",
                    "...but it'll get way harder.",
                    "I'll have your back."
                }
            },
            {
                "111_same_old", new[]
                {
                    "Same old, same old."
                }
            },
            {
                "113_not_steady", new[]
                {
                    "Some things in life won't be that steady"
                }
            },
            {
                "114_how_it_works", new[]
                {
                    "You know how this works..."
                }
            },
            {
                "115_good", new[]
                {
                    "Good..."
                }
            },
            {
                "115_luck", new[]
                {
                    "...luck!"
                }
            },
            {
                "117_thin_line", new[]
                {
                    "Sometimes you might just have to walk on a fine line..."
                }
            },
            {
                "120_change", new[]
                {
                    "Some things will change..."
                }
            },
            {
                "135_real_shadow", new[]
                {
                    "The only real shadow...",
                    "...is the darkest one!",
                }
            },
            {
                "141_patient", new[]
                {
                    "Remember:",
                    "be patient",
                }
            },
            {
                "142_low", new[]
                {
                    "go low..."
                }
            },
            {
                "142_or_go_home", new[]
                {
                    "or go home"
                }
            },
            {
                "145_save", new[]
                {
                    "safe",
                    "space"
                }
            },
            {
                "163_real_shadow", new[]
                {
                    "Remember: The only real shadow...",
                    "...is the darkest one!"
                }
            },
            {
                "167_outro", new[]
                {
                    "So you finally did it...",
                    "I'm proud of you... ",
                    "I didn't think you could make it.",
                    "So it's time to say goodbye now!",
                    "Stay safe little rat.",
                    "I'll miss you."
                }
            },
        };
    }
}