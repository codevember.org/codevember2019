﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Controller;
using EnvironmentBehaviour;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Button = UnityEngine.UI.Button;

namespace LevelSpecific
{
    public class LastLevelController : MonoBehaviourSingleton<LastLevelController>
    {
        private static int _lastLevelPlayThroughCounter = 0;

        public WallMonitorFlickerBehaviour WallMonitor;
        public List<string> WallMonitorTexts = new List<string>();
        [Space(20)] public GameObject OverlayPanel;
        public TextMeshProUGUI WindowText;
        public List<string> TextToDisplayInOverlay = new List<string>();
        public GameObject survCam1Light;
        public GameObject survCam2Light;
        public GameObject ClickToContinueTextGameobject;
        public List<Sprite> levelSprites = new List<Sprite>();

        private CanvasGroup _overlayPanelCanvasGroup;
        private Button _overlayButton;
        private Animator _anim;
        private bool _activeLevelDisplayer = false;

        protected override void Start()
        {
            base.Start();
            _anim = GetComponent<Animator>();
            _overlayPanelCanvasGroup = OverlayPanel.GetComponent<CanvasGroup>();
            _overlayButton = OverlayPanel.GetComponent<Button>();


            // for the first completed run, do the following stuff
            if (GlobalGameSettings.CompletedRuns == 0)
            {
                if (WallMonitor.HasTextsToShow())
                {
                    WallMonitor.SetTextsToShow(new[] {WallMonitorTexts[_lastLevelPlayThroughCounter]});
                }

                WallMonitor.ResetScript();
                WallMonitor.ManualUpdate();

                if (_lastLevelPlayThroughCounter == WallMonitorTexts.Count - 1)
                {
                    // Disable surveillance cameras
                    StartCoroutine(DisableCameras());
                }
                else
                {
                    _lastLevelPlayThroughCounter++;
                }
            }
        }

        private IEnumerator DisableCameras()
        {
            yield return new WaitForSeconds(1.5f);
            // Disable light detection
            GameManager.Instance.checkLightCollision = false;
            // Disable cameras, flickering them out
            survCam1Light.SetActive(false);
            survCam2Light.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            survCam1Light.SetActive(true);
            survCam2Light.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            survCam1Light.SetActive(false);
            survCam2Light.SetActive(false);
            yield return new WaitForSeconds(0.4f);
            survCam1Light.SetActive(true);
            survCam2Light.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            survCam1Light.SetActive(false);
            survCam2Light.SetActive(false);
        }

        public IEnumerator StartEndAnimation()
        {
            // Execute different script for second ending
            var musicController = GameManager.Instance.gameObject.GetComponent<MusicController>();
            switch (GlobalGameSettings.CompletedRuns)
            {
                case 0:
                    // Play door animation
                    _anim.Play("OpenDoorFinalLevel");

                    // Wait time for animation to finish (4s) + a lil bit
                    yield return new WaitForSeconds(2f);
                    
                    UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.OnlyTheBeginning);
                    
                    yield return new WaitForSeconds(4f);

                    // If the user wasn't caught yet, give him props:
                    if (GlobalGameSettings.GetCaughtCount() == 0)
                    {
                        UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.Invisible);
                    }

                    OverlayPanel.SetActive(true);
                    // Fade out music
                    if (musicController != null)
                    {
                        musicController.FadeOut(3f);
                    }

                    // Fade to overlay
                    yield return StartCoroutine(_overlayPanelCanvasGroup.FadeCanvasGroupTo(1f, 2f));
                    _overlayPanelCanvasGroup.alpha = 1f;
                    yield return new WaitForSeconds(1.5f);
                    // Show Text in loop
                    foreach (var text in TextToDisplayInOverlay)
                    {
                        WindowText.text = text;
                        yield return new WaitForSeconds(Random.Range(4f, 5f));
                    }

                    WindowText.text = " ";

                    // Set button active
                    _overlayButton.interactable = true;
                    ClickToContinueTextGameobject.SetActive(true);
                    break;

                case 1:
                    // Play door animation
                    _anim.Play("OpenDoorFinalLevel");

                    //start loop already
                    //random loop of levels being shown
                    _activeLevelDisplayer = true;
                    var spritRenderers = FindObjectsOfType<SpriteRenderer>().ToList();
                    StartCoroutine(RandomLevelInMonitorDisplayer(spritRenderers));

                    // Wait time for animation to finish (4s) + a lil bit
                    yield return new WaitForSeconds(2f);
                    UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.ConsistentCharacter);
                    
                    yield return new WaitForSeconds(4f);

                    // If the user wasn't caught yet, give him props:
                    if (GlobalGameSettings.GetCaughtCount() == 0)
                    {
                        UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.Invincible);
                    }

                    musicController.FadeOut(2f);

                    //disable player outline
                    GameManager.Instance.GetCurrentPlayer().GetComponent<Outline>().enabled = false;

                    //zoom out camera to show ovservation room
                    //move camera to desired position with animation
                    var cameraAnimator = Camera.main.GetComponent<Animator>();
                    cameraAnimator.enabled = true;
                    cameraAnimator.Play("LastLevelCameraMove");

                    yield return new WaitForSeconds(15f);

                    OverlayPanel.SetActive(true);
                    StartCoroutine(_overlayPanelCanvasGroup.FadeCanvasGroupTo(1f, 1.5f));
                    musicController.FadeOut(2.5f);

                    yield return new WaitForSeconds(3f);

                    _activeLevelDisplayer = false;
                    cameraAnimator.enabled = false;

                    GameManager.Instance.LoadCreditsScene();

                    break;
            }

            _lastLevelPlayThroughCounter = 0;
        }

        private IEnumerator FinishSequence()
        {
            _overlayButton.interactable = false;

            yield return null;

            //switch scene to transition scene
            SceneManager.LoadScene(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.TransitionScene]);
        }

        public void OnClickOverlay()
        {
            StartCoroutine(FinishSequence());
        }

        private IEnumerator RandomLevelInMonitorDisplayer(List<SpriteRenderer> list)
        {
            var startTimer = Random.Range(0.01f, 1f);
            var rndIndex = Random.Range(0, list.Count);
            var rndSprite = levelSprites[Random.Range(0, levelSprites.Count)];
            yield return new WaitForSeconds(startTimer);
            list[rndIndex].sprite = rndSprite;
            if (_activeLevelDisplayer) StartCoroutine(RandomLevelInMonitorDisplayer(list));
        }
    }
}