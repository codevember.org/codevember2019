﻿using Controller;
using LevelSpecific.ChapterSelection;
using UnityEngine;
using UnityEngine.UI;

namespace LevelSpecific
{
    public class LevelSelectionScript : MonoBehaviour
    {
        public int levelNumber;

        private Image _levelThumbnailImage;
        private Button _levelButton;

        public void Start()
        {
            // Check if this btn needs to be unlocked
            _levelButton = GetComponent<Button>();
            _levelThumbnailImage = transform.GetChild(0).GetComponent<Image>();
            UnlockCheck();
        }

        private void UnlockCheck()
        {
            if (levelNumber > GlobalGameSettings.GetLastUnlockedLevelNumber() && !GameManager.Instance.DEV_MODE)
            {
                // Lock it and don't register button click
                _levelButton.interactable = false;
                // ... and set image
                _levelThumbnailImage.sprite = ChapterController.Instance.lockedLevelThumbnail;
            }
            else
            {
                // Needs to be unlocked
                _levelButton.interactable = true;
                // ... and register button click
                _levelButton.onClick.AddListener(OnButtonClick);
            }
        }

        private void OnButtonClick()
        {
            ChapterController.Instance.StartSelectedLevel(levelNumber);
        }
    }
}