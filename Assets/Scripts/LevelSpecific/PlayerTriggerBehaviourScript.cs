﻿using Controller;
using UnityEngine;

namespace LevelSpecific
{
    public class PlayerTriggerBehaviourScript : MonoBehaviour
    {
        private GameManager _gameManager;
        private CameraController _cameraController; // TODO: Cleaner access?

        private void Awake()
        {
            _gameManager = GameManager.Instance;
        }

        private void Start()
        {
            _cameraController =
                FindObjectOfType<CameraController>(); // Currently we use only one Camera... TODO: Make sure it's the right one
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Finish"))
            {
                other.enabled = false;
                _gameManager.PlayerReachedFinish();
            }
            else if (other.CompareTag("MoveCamera"))
            {
                _cameraController.FollowPlayer();
            }
            else if (other.CompareTag("EndMoveCamera"))
            {
                _cameraController.LockCamera();
            }
            else if (other.CompareTag("ChromaticAberration"))
            {
                // Trigger camera effect...
                FindObjectOfType<PostProcessingCameraChecker>().TriggerChromaticAberrationImpulse();
                // ... and play sound
                _gameManager.GetCurrentPlayer().GetComponent<Player>().DistortionSoundEffect();
                // Trigger achievement
                UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.StayCalmAndBreathe);
            }
            else if (other.CompareTag("GameFinish"))
            {
                //_cameraController.LockCamera();
                _gameManager.PlayerReachedGameFinish();
            }
            else
            {
                Debug.Log("PlayerTriggerBehaviourScript, Unknown Tag:" + other.tag);
            }
        }
    }
}