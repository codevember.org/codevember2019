﻿using UnityEngine;

namespace Controller
{
    public class SimpleCharController : MonoBehaviour
    {
        public float movementSpeed = 0.5f;
    
        // Update is called once per frame
        private void FixedUpdate()
        {
        
            float verticalInput = 0;
            float horizontalInput = 0;
        
            if (Input.GetKeyDown(KeyCode.A))
            {
                horizontalInput--;
            }
        
            if (Input.GetKeyDown(KeyCode.D))
            {
                horizontalInput++;
            }
        
            if (Input.GetKeyDown(KeyCode.W))
            {
                verticalInput++;
            }
        
            if (Input.GetKeyDown(KeyCode.S))
            {
                verticalInput--;
            }

            var transform1 = transform;
            transform1.position = transform1.position + new Vector3(horizontalInput * movementSpeed, 0, verticalInput * movementSpeed);
        }
    }
}
