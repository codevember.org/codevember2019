﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Controller
{
    public class UiController : MonoBehaviourSingleton<UiController>
    {
        public AudioSource UiSound;
        
        public GameObject finishScreen;
        public GameObject restartScreen;
        
        public Text caughtCounterText;
        
        public GameObject achievementBox;
        public Text achievementTitle;
        public Text achievementText;

        private bool isShowingAchievement = false;

        protected override void Start()
        {
            // Set caught count
            var caughtCount = GlobalGameSettings.GetCaughtCount();
            if (caughtCount == 0)
            {
                caughtCounterText.text = "";
            }
            else
            {
                caughtCounterText.text = "Times caught: " + caughtCount;
                StartCoroutine(WaitAndHide(3, caughtCounterText.gameObject));
            }

            // Disable achievement box
            achievementBox.SetActive(false);
        }

        public void TryActivateAndPersistAchievement(GlobalGameSettings.Achievement achievement)
        {
            if (GlobalGameSettings.IsAchievementDone(achievement)) return;

            StartCoroutine(WaitAndShow(achievement));
        }
        
        private IEnumerator WaitAndHide(int time, GameObject gameObjectToHide)
        {
            var fadetime = 0.3f;
            yield return new WaitForSeconds(time - fadetime);
            if (gameObjectToHide.GetComponent<CanvasGroup>())
            {
                StartCoroutine(gameObjectToHide.GetComponent<CanvasGroup>().FadeCanvasGroupTo(0f, fadetime));
                yield return new WaitForSeconds(fadetime);
            }
            gameObjectToHide.SetActive(false);
        }

        private IEnumerator AnimateAchievementBox(float time)
        {
            var startScale = Vector3.zero;
            var targetScale = Vector3.one;
            var bounceMultiplier = 1.2f;

            achievementBox.GetComponent<CanvasGroup>().alpha = 1f;
            achievementBox.transform.localScale = startScale;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / (time / 2);
                achievementBox.transform.localScale = Vector3.Lerp(startScale, targetScale * bounceMultiplier, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, t)));
                yield return null;
            }

            t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / (time / 2);
                achievementBox.transform.localScale = Vector3.Lerp(targetScale * bounceMultiplier, targetScale, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, t)));
                yield return null;
            }

            yield return new WaitForSeconds(time);
        }

        private IEnumerator WaitAndShow(GlobalGameSettings.Achievement achievement)
        {
            isShowingAchievement = true;
            var fadetime = 0.3f;
            yield return new WaitForSeconds(0.3f);
            achievementBox.SetActive(true);
            achievementBox.GetComponent<CanvasGroup>().alpha = 0;
            UiSound.Play();

            Debug.Log("ActivateAndPersistAchievement" + achievement);
            
            // Update UI
            achievementTitle.text = GlobalGameSettings.GetAchievementTitle(achievement);
            achievementText.text = GlobalGameSettings.GetAchievementDescription(achievement);
            achievementBox.SetActive(true);

            // Let box plopp in
            yield return StartCoroutine(AnimateAchievementBox(fadetime));

            // ... and persist
            GlobalGameSettings.FlagAchievementAsDone(achievement);

            var showingTime = 3;
            StartCoroutine(WaitAndHide(showingTime, achievementBox));
            yield return new WaitForSeconds(showingTime);
            isShowingAchievement = false;
        }

        public void StartLevelFinishScreen()
        {
            GameManager.Instance.EnablePlayerMovement(false);
            finishScreen.SetActive(true);
            
            // Show the achievement if available
            if (GameManager.Instance.GetCurrentLevelNumber() != 1)
            {
                Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.StayOnTheDarkSide);
            }
        }

        public void StartLevelRestartScreen()
        {
            GameManager.Instance.EnablePlayerMovement(false);
            restartScreen.SetActive(true);
        }

        public void CloseFinishInterface()
        {
            if (isShowingAchievement) return;

            UiSound.Play();
            
            GameManager.Instance.SceneTransition = true;
            finishScreen.SetActive(false);
            GameManager.Instance.PrepareAndStartNextLevel();
        }

        public void CloseRestartInterface(bool restart)
        {
            if (isShowingAchievement) return;

            UiSound.Play();
            restartScreen.SetActive(false);

            if (restart)
            {
                StartCoroutine(RestartScene());
            }
            else
            {
                Debug.LogError("ToDo: Go to main menu");
            }
        }

        private IEnumerator RestartScene()
        {
            // Note: This prevents the menu sound from being `cut off`
            yield return new WaitForSeconds(0.1f);
            GameManager.Instance.RestartScene();
        }

        public void OnClickBack()
        {
            UiSound.Play();
            GameManager.Instance.OpenMainMenu();
        }
    }
}