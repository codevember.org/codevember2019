﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LevelSpecific;
using LevelSpecific.ChapterSelection;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Controller
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public bool DEV_MODE = false;

        private const float PlayerMoveSpeed = 20f;

        private int _levelNumber;

        public bool SceneTransition
        {
            set
            {
                if (_blackScreenImage) _blackScreenImage.enabled = value;
                //EnablePlayerMovement(false);
            }
        }

        private Image _blackScreenImage;

        private UiState _activeUiState = UiState.Playing;
        public bool checkLightCollision = true;

        private GameObject _currentPlayer;
        private Rigidbody _currentPlayerRigidbody;
        private Transform _currentPlayerTransform;
        private Transform _currentPlayerBodyTransform;
        private Player _currentPlayerScript;
        private JoystickInputController _joystickInputController;
        private MusicController _musicController;
        private int PlayableLevelsCount => GlobalGameSettings.LevelPrefabPathList.Count;

        private int _latestChapter;
        private GameObject _loadedPrefabResource = null;

        public enum UiState
        {
            Menu,
            Paused,
            Playing,
            Editor // Note: This mode is currently not used and should enable the moving of movable items
        }

        public UiState GetActiveUiState()
        {
            return _activeUiState;
        }

        public void LoadScene(int levelNumber)
        {
            GlobalGameSettings.LevelGameObject = null;
            // Load the unity game scene...
            _levelNumber = levelNumber;
            SceneManager.LoadScene(GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.GameScene]);
            var currentChapterNr = GlobalGameSettings.GetLatestChapter(_levelNumber);

            // ...and play music according to the chapter nummer
            if ((_latestChapter != currentChapterNr || _latestChapter == -1) && _musicController != null)
            {
                _musicController.PlayChapterMusic(currentChapterNr);
            }
        }

        public int GetCurrentLevelNumber()
        {
            return _levelNumber;
        }

        // Loads the level prefab into the game scene
        public void LoadPrefabIntoScene()
        {
            // Init joystick controls
            if (_joystickInputController == null)
            {
                _joystickInputController = GameObject.FindObjectOfType<JoystickInputController>();
            }

            // For debugging purposes if there is already an level in scene
            if (!GameObject.FindGameObjectWithTag("Level"))
            {
                // ... convert number to index and get prefab from list
                _loadedPrefabResource =
                    LoadPrefabFromFile(GlobalGameSettings.LevelPrefabPathList[Math.Max(_levelNumber - 1, 0)]) as
                        GameObject;
                Instantiate(_loadedPrefabResource, Vector3.zero, Quaternion.identity);
                GlobalGameSettings.LevelGameObject = _loadedPrefabResource;
                _latestChapter = GlobalGameSettings.GetLatestChapter(_levelNumber);
            }

            var iteration = GlobalGameSettings.GetLastPlayedLevelIteration();
            Debug.Log("Level iteration: " + iteration + ", level number: " + _levelNumber);

            // Level based achievements
            if (_levelNumber == 15)
            {
                UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.StoryTeaser);
            }
            else if (GlobalGameSettings.CurrentStoryLoop == GlobalGameSettings.StoryLoop.Hard && _levelNumber == 3)
            {
                UiController.Instance.TryActivateAndPersistAchievement(GlobalGameSettings.Achievement.LoosingMyMind);
            }

            _currentPlayer = GameObject.FindGameObjectWithTag("Player");
            _currentPlayerRigidbody = _currentPlayer.GetComponent<Rigidbody>();
            _currentPlayerScript = _currentPlayer.GetComponent<Player>();
            _currentPlayerTransform = _currentPlayer.transform;
            _currentPlayerBodyTransform = _currentPlayer.transform.Find("Body").transform;

            // ToDo: The init logic may want to be refactored
            _activeUiState = UiState.Playing;
            var lightCollisionBehaviour = _currentPlayer.GetComponent<LightCollisionBehaviour>();
            if (lightCollisionBehaviour) lightCollisionBehaviour.UpdateSurveillanceCameraLights();
            EnablePlayerMovement(true);

            // All together now: READY TO PLAY
            SceneTransition = false;

            // Increase the current iteration if the same level is played again
            if (GlobalGameSettings.GetLastPlayedLevelNumber() == _levelNumber)
            {
                GlobalGameSettings.IncreaseLastPlayedLevelIteration();
            }

            GlobalGameSettings.SaveLastPlayedLevelNumber(_levelNumber);
        }

        // For internal use only: Force load a specific number
        public void LoadPrefabIntoScene(string levelName)
        {
            // Init joystick controls
            if (_joystickInputController == null)
            {
                _joystickInputController = GameObject.FindObjectOfType<JoystickInputController>();
            }

            // For debugging purposes if there is already an level in scene
            if (!GameObject.FindGameObjectWithTag("Level"))
            {
                // ... convert number to index and get prefab from list
                _loadedPrefabResource =
                    LoadPrefabFromFile(levelName) as GameObject;
                Instantiate(_loadedPrefabResource, Vector3.zero, Quaternion.identity);
                GlobalGameSettings.LevelGameObject = _loadedPrefabResource;
            }

            _currentPlayer = GameObject.FindGameObjectWithTag("Player");
            _currentPlayerRigidbody = _currentPlayer.GetComponent<Rigidbody>();
            _currentPlayerScript = _currentPlayer.GetComponent<Player>();
            _currentPlayerTransform = _currentPlayer.transform;
            _currentPlayerBodyTransform = _currentPlayer.transform.Find("Body").transform;

            // ToDo: The init logic may want to be refactored
            _activeUiState = UiState.Playing;
            var lightCollisionBehaviour = _currentPlayer.GetComponent<LightCollisionBehaviour>();
            if (lightCollisionBehaviour) lightCollisionBehaviour.UpdateSurveillanceCameraLights();
            EnablePlayerMovement(true);

            // All together now: READY TO PLAY
            SceneTransition = false;
        }

        public void RestartScene()
        {
            LoadScene(_levelNumber);
        }

        public void PrepareAndStartNextLevel()
        {
            ++_levelNumber;
            GlobalGameSettings.ResetLastPlayedLevelIteration();
            // First: Safety check
            if (_levelNumber > PlayableLevelsCount)
            {
                Debug.LogError("No more level to play, do nothing...");
            }
            else
            {
                LoadScene(_levelNumber);
            }
        }

        protected override void Awake()
        {
            // Set fps and disable v-sync for better performance
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 40;

            DontDestroyOnLoad(gameObject);
            base.Awake();
        }

        protected override void Start()
        {
            _blackScreenImage = GetComponentInChildren<Image>();
            _musicController = gameObject.GetComponent<MusicController>();
            if (_musicController != null)
            {
                _musicController.MuteAudioSource(GlobalGameSettings.AudioMuted());
            }

            base.Start();

            // Apply currently set graphic settings initially
            GlobalGameSettings.ApplyGraphicsQuality();
        }

        public void OpenMainMenu()
        {
            SceneManager.LoadScene(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.MainMenuScene]);
            _musicController.PlayChapterMusic(-1);
            _latestChapter = -1;
        }

        public void EnablePlayerMovement(bool enable)
        {
            //Debug.Log("GameManager#EnablePlayerMovement: " + enable);
            _joystickInputController.EnableMovement(enable);
        }

        public void MovePlayer(float x, float z)
        {
            _currentPlayerRigidbody.AddForce(new Vector3(x * PlayerMoveSpeed, 0, z * PlayerMoveSpeed));
            // Note: The coordinates are rotated around 90 degrees so the player faces actual forward
            _currentPlayerBodyTransform.LookAt(_currentPlayerBodyTransform.position + new Vector3(-z, 0, x));
        }

        public void StopMovement()
        {
            // Stop rotation
            if (_currentPlayerRigidbody != null) _currentPlayerRigidbody.angularVelocity = Vector3.zero;
        }

        public bool ToggleMuteAudio()
        {
            var muted = GlobalGameSettings.ToggleMuteAudio();
            // Trigger mute in player prefs und mute audio source
            _musicController.MuteAudioSource(GlobalGameSettings.AudioMuted());
            return muted;
        }

        private static UnityEngine.Object LoadPrefabFromFile(string filename)
        {
            var fileNameAccordingToLevelLoop = GlobalGameSettings.GetLevelNameForCurrentStoryLoop(filename);

            Debug.Log("Trying to load LevelPrefab from file (" + fileNameAccordingToLevelLoop + ")...");
            var loadedObject = Resources.Load("Levels/" + fileNameAccordingToLevelLoop);
            if (loadedObject == null)
            {
                throw new FileNotFoundException("...no file found - " +
                                                "please check the configuration within the `GameManager`!");
            }

            return loadedObject;
        }

        #region Interfaces

        public void PlayerDied()
        {
            _currentPlayerScript.Die();
            UiController.Instance.StartLevelRestartScreen();
            Debug.LogWarning("Player died");
            _activeUiState = UiState.Paused;
            GlobalGameSettings.IncreaseCaughtCount();
        }

        public void PlayerReachedFinish()
        {
            _currentPlayerScript.ReachedFinish();
            UiController.Instance.StartLevelFinishScreen();
            _activeUiState = UiState.Paused;

            var nextChapter = GlobalGameSettings.GetNextChapter(_levelNumber);
            if (nextChapter != _latestChapter) _musicController.FadeOut(0.5f);
        }

        public void PlayerReachedGameFinish()
        {
            StartCoroutine(PlayerReachedFinishCo());
        }

        private IEnumerator PlayerReachedFinishCo()
        {
            // Deactivate player input
            EnablePlayerMovement(false);

            // Kill player move animation
            Player.IsMoving = false;

            // Wait for camera to catch up
            yield return new WaitForSeconds(1f);
            // ... and let the script to da magic
            yield return StartCoroutine(LastLevelController.Instance.StartEndAnimation());
        }

        public void LoadCreditsScene()
        {
            _musicController.PlayChapterMusic(-2);
            _latestChapter = -1;

            SceneManager.LoadScene(
                GlobalGameSettings.SceneStringDictionary[GlobalGameSettings.SceneString.EndingScene]);
        }

        public GameObject GetCurrentPlayer()
        {
            return _currentPlayer;
        }

        #endregion Interface
    }
}