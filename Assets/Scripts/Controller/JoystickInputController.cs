﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Controller
{
    public class JoystickInputController : MonoBehaviour
    {
        // Defines the maximal movement range / input range of the joystick knob 
        private const float MaxKnobMoveInputRestriction = 110f; // TODO: this has to be resolution/density based

        public float speed = 0.05f;
        [HideInInspector] public bool ReachedMaxVelocity = false;
        public bool TouchStart => _touchStart;
        private bool _touchStart = false;
        private Vector2 _touchStartPoint;
        private Vector2 _pointB;

        public Transform knob;
        private RectTransform _knobRectTransform;
        private Image _knobImage;

        public Transform outerCircle;
        private RectTransform _outerCircleRectTransform;

        private Image _outerCircleImage;

        //when controls are locked, place them somewhere specific
        private bool _isInLockedControlsMode = false;
        private Vector2 _lockedJoystickPositionLeft;
        private Vector2 _lockedJoystickPositionRight;
        private Vector2 _lastedLockedPosition;

        private bool _enabledState = false;

        public bool MovementEnableCheck()
        {
            return _enabledState;
        }

        public void EnableMovement(bool enable)
        {
            _enabledState = enable;
            if (!enable)
            {
                if (_knobImage) _knobImage.enabled = false;
                if (_outerCircleImage) _outerCircleImage.enabled = false;
            }
        }

        private void Start()
        {
            _isInLockedControlsMode = GlobalGameSettings.GetLockedControls();
            _lockedJoystickPositionLeft = new Vector2(Screen.width / 5f, Screen.height / 4f);
            _lockedJoystickPositionRight = new Vector2(Screen.width / 5f * 4f, Screen.height / 4f);

            // Set the last locked control position to the one the user used
            _lastedLockedPosition = GlobalGameSettings.GetLastLockedControlsPosition() == "right"
                ? _lockedJoystickPositionRight
                : _lockedJoystickPositionLeft;

            if (knob)
            {
                _knobRectTransform = knob.GetComponent<RectTransform>();
                _knobImage = knob.GetComponent<Image>();
            }

            if (outerCircle)
            {
                _outerCircleRectTransform = outerCircle.GetComponent<RectTransform>();
                _outerCircleImage = outerCircle.GetComponent<Image>();
            }
        }

        private void Update()
        {
            if (!_enabledState) return;
            if (Input.GetMouseButtonDown(0))
            {
                _touchStartPoint = Input.mousePosition;
                if (_isInLockedControlsMode)
                {
                    if (_touchStartPoint.x < Screen.width / 2f)
                    {
                        _lastedLockedPosition = _lockedJoystickPositionLeft;
                        GlobalGameSettings.SetLastLockedControlsPosition("left");
                    }
                    else
                    {
                        _lastedLockedPosition = _lockedJoystickPositionRight;
                        GlobalGameSettings.SetLastLockedControlsPosition("right");
                    }

                    EnableKnob(true, _lastedLockedPosition, _lastedLockedPosition);
                }
                else
                {
                    EnableKnob(true, _touchStartPoint, _touchStartPoint);
                }
            }

            if (Input.GetMouseButton(0))
            {
                _touchStart = true;
                _pointB = Input.mousePosition;
            }
            else
            {
                _touchStart = false;
            }
        }

        private void FixedUpdate()
        {
            if (!_enabledState) return;

            if (_touchStart)
            {
                var origin = _touchStartPoint;

                if (_isInLockedControlsMode)
                {
                    if (_touchStartPoint.x < Screen.width / 2f)
                    {
                        origin = _lockedJoystickPositionLeft;
                    }
                    else
                    {
                        origin = _lockedJoystickPositionRight;
                    }
                }

                var offset = _pointB - origin;
                var direction = Vector2.ClampMagnitude(offset, MaxKnobMoveInputRestriction);
                var normalizedDirection = direction / MaxKnobMoveInputRestriction;
                MoveCharacter(normalizedDirection); // Normalize
                //todo: limit knob pos to start at center somehow

                _knobRectTransform.position = new Vector2(origin.x + direction.x / 3, origin.y + direction.y / 3);

                // Set reachedmaxvel when it is reached (tolerance of 0.01)
                ReachedMaxVelocity = Math.Abs(Mathf.Abs(normalizedDirection.x) - 1f) < 0.01f;
            }
            else
            {
                if (!_isInLockedControlsMode)
                {
                    EnableKnob(false, Vector2.zero, Vector2.zero);
                }
                else
                {
                    EnableKnob(false, _lastedLockedPosition, _lastedLockedPosition);
                }

                Player.IsMoving = false;
                GameManager.Instance.StopMovement();
            }
        }

        private void MoveCharacter(Vector2 direction)
        {
            Player.IsMoving = true;
            var movement = direction * speed * Time.deltaTime;
            GameManager.Instance.MovePlayer(movement.x, movement.y);
        }

        private void EnableKnob(bool enable, Vector2 knobPos, Vector2 circlePos)
        {
            //check if player decided to lock controls
            if (_isInLockedControlsMode)
            {
                _knobRectTransform.position = knobPos;
                _outerCircleRectTransform.position = circlePos;
                _knobImage.enabled = true;
                _outerCircleImage.enabled = true;
            }
            else
            {
                if (enable)
                {
                    _knobRectTransform.position = knobPos;
                    _outerCircleRectTransform.position = circlePos;
                }

                _knobImage.enabled = enable;
                _outerCircleImage.enabled = enable;
            }
        }
    }
}