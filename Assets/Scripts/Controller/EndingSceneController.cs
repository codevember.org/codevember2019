﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controller
{
    public class EndingSceneController : MonoBehaviour
    {
        public GameObject credits;
        public CanvasGroup blackOverlay;

        private AudioSource _audioSource;

        void Start()
        {
            //audio source for sound effects
            _audioSource = gameObject.GetComponent<AudioSource>();
            if (!GlobalGameSettings.AudioMuted())
            {
                _audioSource.volume = 0;
                // Fade in audio
                StartCoroutine(_audioSource.FadeAudioSource(0.4f, 2f));
            }

            StartCoroutine(EndCredits());
        }

        private IEnumerator EndCredits()
        {
            // start camera movement in scene
            StartCoroutine(MoveGameObject(Camera.main.gameObject, new Vector3(25f, 0, 0), 50f));

            yield return new WaitForSeconds(0.5f);
            // starts with black overlay
            StartCoroutine(blackOverlay.FadeCanvasGroupTo(0f, 2f));
            yield return new WaitForSeconds(2f);

            //slowly roll credits up
            yield return StartCoroutine(MoveGameObject(credits, new Vector3(0, 7000f, 0), 40f));
            yield return new WaitForSeconds(1f);

            // starts with black overlay
            StartCoroutine(blackOverlay.FadeCanvasGroupTo(1f, 2f));
            // Fadeout audio
            StartCoroutine(_audioSource.FadeAudioSource(0f, 2f));

            yield return new WaitForSeconds(3f);
            GameManager.Instance.OpenMainMenu();
        }

        private IEnumerator MoveGameObject(GameObject go, Vector3 direction, float time)
        {
            var i = 0.0f;
            var rate = 1.0f / time;
            var goStartPos = go.transform.position;
            while (i < 1.0)
            {
                i += Time.deltaTime * rate;
                // Lerp between pos
                go.transform.position = Vector3.Lerp(goStartPos,
                    goStartPos + direction, i);
                yield return null;
            }

            yield return null;
        }
    }
}