﻿using System.Collections;
using System.Collections.Generic;
using Controller;
using UnityEngine;

public class AchievementController : MonoBehaviour
{
    public Transform achievementContainer;
    public GameObject achievementPrefabs;

    private GlobalGameSettings.Achievement[] _allAchievements => GlobalGameSettings.GetAllAchievements();

    public void OnEnable()
    {
        if (achievementContainer.childCount != 0) return;
        foreach (var achievement in _allAchievements)
        {
            var achObj = Instantiate(achievementPrefabs, achievementContainer);
            achObj.GetComponent<AchievementObject>().SetupAchievement(achievement);
        }
    }
}