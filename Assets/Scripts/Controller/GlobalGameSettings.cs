﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Controller
{
    public static class GlobalGameSettings
    {
        #region LevelList

        // Defines the list and order of the playable levels
        public static readonly List<string> LevelPrefabPathList = new List<string>
        {
            // Intro and chapter 1
            "010_EscapeLevel",
            "011_Level",
            "012_Level",
            "013_Level",
            "014_Level",
            "015_Level",
            "016_Level",
            "017_Level",
            // Chapter 2
            "020_Level",
            "021_Level",
            "022_Level",
            "023_Level",
            "024_Level",
            "025_Level",
            "026_Level",
            "027_Level",
            // Chapter 3
            "030_Level",
            "031_Level",
            "032_Level",
            "033_Level",
            "034_Level",
            "035_Level",
            "036_Level",
            "037_Level",
            // Chapter 4
            "040_Level",
            "041_Level",
            "042_Level",
            "043_Level",
            "044_Level",
            "045_Level",
            // Chapter 5
            "050_Level",
            "051_Level",
            "052_Level",
            "053_Level",
            "054_Level",
            // Chapter 6
            "060_Level",
            "061_Level",
            "062_Level",
            "063_Level",
            "067_Level",
            // TODO
            //"999_EndingLevel"
        };

        /** Returns the chapter according to a level string. Returns -1 if there was no matching chapter found. */
        public static int GetChapterForLevelName(string levelName)
        {
            if (levelName.StartsWith("01")) return 1;
            if (levelName.StartsWith("02")) return 2;
            if (levelName.StartsWith("03")) return 3;
            if (levelName.StartsWith("04")) return 4;
            if (levelName.StartsWith("05")) return 5;
            if (levelName.StartsWith("06")) return 6;

            return -1;
        }

        /** Returns the chapter according to a level string. Returns -1 if there was no matching chapter found. */
        public static string GetLevelNameForCurrentStoryLoop(string levelName)
        {
            switch (CurrentStoryLoop)
            {
                case StoryLoop.Hard: return "1" + levelName.Substring(1);
                default: return levelName; // Also: StoryLoop.Easy
            }
        }

        /** Returns the chapter of the level number **/
        public static int GetNextChapter(int levelNumber)
        {
            return GetChapterForLevelName(LevelPrefabPathList[(levelNumber + 1) - 1]);
        }

        /** Returns the latest chapter **/
        public static int GetLatestChapter(int levelNumber)
        {
            if (levelNumber == 0)
            {
                Debug.Log("Level " + levelNumber + " is the first Level");
                return 0;
            }
            else if (levelNumber < 0)
            {
                Debug.LogError("Level number requested '" + levelNumber + "' should not be negative!");
                return 0;
            }

            return GetChapterForLevelName(LevelPrefabPathList[levelNumber - 1]);
        }

        #endregion

        #region SceneAssignment

        public enum SceneString
        {
            MainMenuScene,
            IntroScene,
            GameScene,
            TransitionScene,
            EndingScene,
        }

        public static readonly Dictionary<SceneString, string> SceneStringDictionary =
            new Dictionary<SceneString, string>()
            {
                {SceneString.MainMenuScene, "01MainMenu"},
                {SceneString.IntroScene, "02IntroScene"},
                {SceneString.GameScene, "03GameScene"},
                {SceneString.EndingScene, "04EndingScene"},
                {SceneString.TransitionScene, "05TransitionScene"}
            };

        #endregion

        #region LevelSettings

        private const string LastPlayedLevelString = "LastPlayedLevel";
        private const string LastPlayedLevelIterationString = "LastPlayedLevelIteration";
        private const string LastUnlockedLevelString = "LastUnlockedLevel";

        // Iteration related block
        public static int GetLastPlayedLevelIteration()
        {
            return PlayerPrefs.GetInt(LastPlayedLevelIterationString, 0);
        }

        public static void SetLastPlayedLevelIteration(int iteration)
        {
            PlayerPrefs.SetInt(LastPlayedLevelIterationString, iteration);
        }

        public static void ResetLastPlayedLevelIteration()
        {
            PlayerPrefs.SetInt(LastPlayedLevelIterationString, 0);

            // Reset fluid difficulty
            switch (CurrentStoryLoop)
            {
                case StoryLoop.Hard:
                    InitialLightCollisionThreshold = LightCollisionThresholdHard;
                    break;
                case StoryLoop.Easy:
                    InitialLightCollisionThreshold = LightCollisionThresholdEasy;
                    break;
            }
        }

        public static void IncreaseLastPlayedLevelIteration()
        {
            var currentLevelIteration = GetLastPlayedLevelIteration() + 1;

            // Update fluid difficulty: If a user gets stuck, make the game easier
            Debug.Log("Current level iteration: " + currentLevelIteration);
            switch (currentLevelIteration)
            {
                case 5:
                    InitialLightCollisionThreshold = InitialLightCollisionThreshold + 5;
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.TakeItEasy);
                    break;
                case 10:
                    InitialLightCollisionThreshold = InitialLightCollisionThreshold + 5;
                    break;
                case 25:
                    InitialLightCollisionThreshold = InitialLightCollisionThreshold + 5;
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.HardNerves);
                    break;
                case 50:
                    InitialLightCollisionThreshold = InitialLightCollisionThreshold + 5;
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.Kudos);
                    break;
                case 100:
                    InitialLightCollisionThreshold = InitialLightCollisionThreshold + 10;
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.WhySoSerious);
                    break;
            }

            PlayerPrefs.SetInt(LastPlayedLevelIterationString, currentLevelIteration);
        }
        // End iteration related block

        public static void SaveLastPlayedLevelNumber(int levelNumber)
        {
            PlayerPrefs.SetInt(LastPlayedLevelString, levelNumber);
            UpdateLastUnlockedLevelNumber(levelNumber);
        }

        public static void UpdateLastUnlockedLevelNumber(int levelNumber)
        {
            // Only update the last unlocked level with the highest number
            var lastUnlockedLevel = PlayerPrefs.GetInt(LastUnlockedLevelString);
            PlayerPrefs.SetInt(LastUnlockedLevelString, Math.Max(lastUnlockedLevel, levelNumber + 1));
        }

        public static void ResetLastUnlockedLevelNumber()
        {
            PlayerPrefs.SetInt(LastUnlockedLevelString, 0);
        }

        public static int GetLastPlayedLevelNumber()
        {
            return PlayerPrefs.GetInt(LastPlayedLevelString);
        }

        public static int GetLastUnlockedLevelNumber()
        {
            var levelNumber = PlayerPrefs.GetInt(LastUnlockedLevelString);
            if (levelNumber == 0) levelNumber = 1;
            return levelNumber;
        }

        public static void PrepareForNewGame()
        {
            // Reset completed runs
            CompletedRuns = 0;
            // Reset diff
            SetDifficultyTo(StoryLoop.Easy);
            // Reset latest unlocked level
            SaveLastPlayedLevelNumber(0);
            ResetLastUnlockedLevelNumber();
            ResetLastPlayedLevelIteration();
            // Reset scores and counter
            ResetCaughtCount();
            // Note: You don't want to reset the achievements :)
        }

        private const string LockedControlsString = "LockedControls";
        private const string LastLockedControlsPositionString = "LastLockedControlsPosition";

        public static bool GetLockedControls()
        {
            return PlayerPrefs.GetInt(LockedControlsString, 1) == 1;
        }

        // 0 == 
        public static string GetLastLockedControlsPosition()
        {
            return PlayerPrefs.GetString(LastLockedControlsPositionString, "left");
        }

        public static void SetLastLockedControlsPosition(string position)
        {
            if (position != "left" && position != "right")
            {
                Debug.LogError("This is not a valid position: " + position);
            }

            PlayerPrefs.SetString(LastLockedControlsPositionString, position);
        }

        public static int ToggleLockedControls()
        {
            PlayerPrefs.SetInt(LockedControlsString, GetLockedControls() ? 0 : 1);
            return GetLockedControls() ? 0 : 1;
        }

        #endregion

        #region AudioSettings

        public const float MaxAudioVolume = 0.6f;
        private const string AudioMutedString = "AudioMuted";

        public static bool ToggleMuteAudio()
        {
            var newState = !AudioMuted();
            PlayerPrefs.SetInt(AudioMutedString, newState ? 1 : 0);
            return newState;
        }

        public static bool AudioMuted()
        {
            return PlayerPrefs.GetInt(AudioMutedString) == 1;
        }

        #endregion

        #region QualitySettings

        public enum Quality
        {
            Medium, // 0
            Low, // 1
            High // 2
        }

        private static Quality GetQualityForInt(int integer)
        {
            switch (integer)
            {
                case 0: return Quality.Medium;
                case 1: return Quality.Low;
                case 2: return Quality.High;
            }

            Debug.LogError("GlobalGameSettings#GetQualityForInt: Invalid value " + integer);
            return Quality.Medium; // Fallback
        }

        private static int GetIntForQuality(Quality quality)
        {
            switch (quality)
            {
                case Quality.Medium: return 0;
                case Quality.Low: return 1;
                case Quality.High: return 2;
            }

            return 0; // Fallback, should never happen
        }

        private const string GraphicsQualityString = "Quality";

        private const int PixelLightCountLow = 5;
        private const int PixelLightCountMedium = 20;
        private const int PixelLightCountHigh = 28;

        private const int ShowDistanceLow = 15;
        private const int ShowDistanceMedium = 20;
        private const int ShowDistanceHigh = 22;

        private const ShadowQuality ShadowQualityLow = ShadowQuality.HardOnly;
        private const ShadowQuality ShadowQualityMedium = ShadowQuality.All;
        private const ShadowQuality ShadowQualityHigh = ShadowQuality.All;

        private const ShadowProjection ShadowProjectionLow = ShadowProjection.CloseFit;
        private const ShadowProjection ShadowProjectionMedium = ShadowProjection.StableFit;
        private const ShadowProjection ShadowProjectionHigh = ShadowProjection.StableFit;

        private const ShadowResolution ShadowResolutionLow = ShadowResolution.Low;
        private const ShadowResolution ShadowResolutionMedium = ShadowResolution.Medium;
        private const ShadowResolution ShadowResolutionHigh = ShadowResolution.High;

        private const int AntiAliasingLow = 0;
        private const int AntiAliasingMedium = 0;
        private const int AntiAliasingHigh = 2;

        public static Quality ToggleGraphicsQuality()
        {
            var newQuality = Quality.Medium;
            switch (GetCurrentQuality())
            {
                case Quality.Low:
                    newQuality = Quality.Medium;
                    break;
                case Quality.Medium:
                    newQuality = Quality.High;
                    break;
                case Quality.High:
                    newQuality = Quality.Low;
                    break;
            }

            PlayerPrefs.SetInt(GraphicsQualityString, GetIntForQuality(newQuality));
            ApplyGraphicsQuality();

            return newQuality;
        }

        public static void ApplyGraphicsQuality()
        {
            // ## Set quality settings ##
            var currentQuality = GetCurrentQuality();
            switch (currentQuality)
            {
                case Quality.Low:
                {
                    QualitySettings.pixelLightCount = PixelLightCountLow;
                    QualitySettings.shadowDistance = ShowDistanceLow;
                    QualitySettings.shadows = ShadowQualityLow;
                    QualitySettings.shadowProjection = ShadowProjectionLow;
                    QualitySettings.shadowResolution = ShadowResolutionLow;
                    QualitySettings.antiAliasing = AntiAliasingLow;
                    break;
                }
                case Quality.Medium:
                {
                    QualitySettings.pixelLightCount = PixelLightCountMedium;
                    QualitySettings.shadowDistance = ShowDistanceMedium;
                    QualitySettings.shadows = ShadowQualityMedium;
                    QualitySettings.shadowProjection = ShadowProjectionMedium;
                    QualitySettings.shadowResolution = ShadowResolutionMedium;
                    QualitySettings.antiAliasing = AntiAliasingMedium;
                    break;
                }
                case Quality.High:
                {
                    QualitySettings.pixelLightCount = PixelLightCountHigh;
                    QualitySettings.shadowDistance = ShowDistanceHigh;
                    QualitySettings.shadows = ShadowQualityHigh;
                    QualitySettings.shadowProjection = ShadowProjectionHigh;
                    QualitySettings.shadowResolution = ShadowResolutionHigh;
                    QualitySettings.antiAliasing = AntiAliasingHigh;
                    break;
                }
            }

            Debug.Log(">>> Set quality to " + currentQuality);
            Debug.Log("Set pixelLightCount to " + QualitySettings.pixelLightCount);
            Debug.Log("Set shadowDistance to " + QualitySettings.shadowDistance);
            Debug.Log("Set shadows to " + QualitySettings.shadows);
            Debug.Log("Set shadowProjection to " + QualitySettings.shadowProjection);
            Debug.Log("Set shadowResolution to " + QualitySettings.shadowResolution);
            Debug.Log("Set antiAliasing to " + QualitySettings.antiAliasing);
        }

        public static Quality GetCurrentQuality()
        {
            return GetQualityForInt(PlayerPrefs.GetInt(GraphicsQualityString));
        }

        public static string GetCurrentQualityString()
        {
            switch (GetCurrentQuality())
            {
                case Quality.Low: return "Quality: Low";
                case Quality.Medium: return "Quality: Medium";
                case Quality.High: return "Quality: High";
            }

            return "Unknown"; // Fallback, should never happen
        }

        #endregion

        #region Scores and Acchievements

        private const string CaughtCountString = "DeathCount";

        // Iteration related block
        public static int GetCaughtCount()
        {
            return PlayerPrefs.GetInt(CaughtCountString, 0);
        }

        public static void IncreaseCaughtCount()
        {
            var currentCount = GetCaughtCount() + 1;

            switch (currentCount)
            {
                case 8:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.OutliveYourPredators);
                    break;
                case 25:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.TwentyFive);
                    break;
                case 42:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.FortyTwo);
                    break;
                case 69:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.TimeToMultiply);
                    break;
                case 111:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.KeepThemBusy);
                    break;
                case 200:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.Ok);
                    break;
                case 418:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.Teapot);
                    break;
                case 666:
                    UiController.Instance.TryActivateAndPersistAchievement(Achievement.SatanicMessages);
                    break;
                default: break;
            }

            PlayerPrefs.SetInt(CaughtCountString, currentCount);
        }

        public static void ResetCaughtCount()
        {
            PlayerPrefs.SetInt(CaughtCountString, 0);
        }

        public enum Achievement
        {
            OnlyTheBeginning, //
            ConsistentCharacter, //
            StayOnTheDarkSide, //
            StayCalmAndBreathe, //
            LoosingMyMind, //
            Invisible, //
            Invincible, //
            StoryTeaser, //

            // Further ideas:
            // Speedy gonzales: finish a level in under x sec
            // Upside down : finish a level with the phone upside down
            // not forgotten : stay close to another mouse for x seconds
            // chore: First try achievement for every level

            // Tries count
            TakeItEasy, //
            HardNerves, //
            Kudos, //
            WhySoSerious, //

            // Caught counts
            OutliveYourPredators, //
            TwentyFive, //
            FortyTwo, //
            TimeToMultiply, //
            KeepThemBusy, //
            Ok, //
            Teapot, //
            SatanicMessages //
        }

        public static Achievement[] GetAllAchievements()
        {
            return (Achievement[]) Enum.GetValues(typeof(Achievement));
        }

        public static string GetAchievementTitle(Achievement achievement)
        {
            switch (achievement)
            {
                case Achievement.OnlyTheBeginning: return "This is only the beginning";
                case Achievement.ConsistentCharacter: return "Consistent character";
                case Achievement.StayOnTheDarkSide: return "Stay on the dark side";
                case Achievement.StayCalmAndBreathe: return "stay calm and breathe";
                case Achievement.LoosingMyMind: return "loosing my mind";
                case Achievement.Invisible: return "Invisible";
                case Achievement.Invincible: return "Invincible - You're a legend";
                case Achievement.StoryTeaser: return "Is there more?";

                // Tries count
                case Achievement.TakeItEasy: return "take it easy";
                case Achievement.HardNerves: return "hard nerves";
                case Achievement.Kudos: return "kudos";
                case Achievement.WhySoSerious: return "why so serious";

                // Caught counts
                case Achievement.OutliveYourPredators: return "outlive your predators";
                case Achievement.TwentyFive: return "Twentyfive";
                case Achievement.FortyTwo: return "42: The answer to everything";
                case Achievement.TimeToMultiply: return "Time to multiply";
                case Achievement.KeepThemBusy: return "Keep them busy";
                case Achievement.Ok: return "200: OK";
                case Achievement.Teapot: return "418: I'm a teapot";
                case Achievement.SatanicMessages: return "satanic messages";

                default: return "Not set yet - Add a description in GetAchievementTitle.";
            }
        }

        public static string GetAchievementDescription(Achievement achievement)
        {
            switch (achievement)
            {
                case Achievement.OnlyTheBeginning: return "Finish game in easy mode";
                case Achievement.ConsistentCharacter: return "Finished game in easy and hard mode";
                case Achievement.StayOnTheDarkSide: return "Play a level without getting caught";
                case Achievement.StayCalmAndBreathe: return "Hit a distraction trigger.";
                case Achievement.LoosingMyMind: return "Play a level in which elements disappear.";
                case Achievement.Invisible: return "Do the whole first run without getting caught";
                case Achievement.Invincible: return "Play the whole game without getting caught - coop style";
                case Achievement.StoryTeaser: return "Take action and keep yourself informed!";

                // Tries count
                case Achievement.TakeItEasy: return "Getting caught in the same level 5 times";
                case Achievement.HardNerves: return "Holy... Getting caught in the same level 25 times";
                case Achievement.Kudos: return "Keep it on! Getting caught in the same level 50 times";
                case Achievement.WhySoSerious:
                    return "Getting caught in the same level 100 times - " +
                           "Maybe try another one or shoot us an email!";

                // Caught counts
                case Achievement.OutliveYourPredators: return "Caught 8 times";
                case Achievement.TwentyFive: return "25 stands for introspective.";
                case Achievement.FortyTwo: return "Getting caught 42 times.";
                case Achievement.TimeToMultiply: return "Getting caught 69 times.";
                case Achievement.KeepThemBusy: return "Getting caught 111 times.";
                case Achievement.Ok: return "Status: Getting caught 200 times";
                case Achievement.Teapot: return "Status: Getting caught 418 times";
                case Achievement.SatanicMessages: return "Weird feeling... Getting caught 666 times";

                default: return "Not set yet - Add a description in GetAchievementDescription.";
            }
        }

        public static bool IsAchievementDone(Achievement achievement)
        {
            return PlayerPrefs.HasKey(achievement.ToString());
        }

        public static void FlagAchievementAsDone(Achievement achievement)
        {
            PlayerPrefs.SetInt(achievement.ToString(), 1);
        }

        public static void ResetAllAchievements()
        {
            foreach (var achievement in GetAllAchievements())
            {
                PlayerPrefs.DeleteKey(achievement.ToString());
            }
        }

        #endregion

        #region Misc Game Settings

        private const string ChangelogSeenString = "ChangelogSeen";
        private const string NotFirstTimePlayingString = "NotFirstTimePlaying";


        public static bool ChangelogSeen
        {
            get => PlayerPrefs.GetInt(ChangelogSeenString) == 1;
            set => PlayerPrefs.SetInt(ChangelogSeenString, value == true ? 1 : 0);
        }

        public static bool NotFirstTimePlaying
        {
            get => PlayerPrefs.GetInt(NotFirstTimePlayingString) == 1;
            set => PlayerPrefs.SetInt(NotFirstTimePlayingString, value == true ? 1 : 0);
        }

        #endregion

        #region StoryLoop

        public enum StoryLoop
        {
            Easy,
            Hard,
            //VeryHard,
        }

        private const string CurrentDifficultyPlayerPrefString = "CurrentStoryLoop";

        public static StoryLoop CurrentStoryLoop
        {
            get
            {
                Enum.TryParse(PlayerPrefs.GetString(CurrentDifficultyPlayerPrefString), out StoryLoop diff);
                return diff;
            }
            private set =>
                PlayerPrefs.SetString(CurrentDifficultyPlayerPrefString, Enum.GetName(value.GetType(), value));
        }

        //## params to adjust ##
        // The frame count a player can get hit until he dies
        private const int LightCollisionThresholdEasy = 30;
        private const int LightCollisionThresholdHard = 25;
        public static int InitialLightCollisionThreshold = LightCollisionThresholdEasy;

        private const string CompletedPlaythroughPlayerPrefString = "CompletedPlaythrough";

        public static int CompletedRuns
        {
            get => PlayerPrefs.GetInt(CompletedPlaythroughPlayerPrefString);
            set => PlayerPrefs.SetInt(CompletedPlaythroughPlayerPrefString, value);
        }

        public static void SetDifficultyTo(StoryLoop diff)
        {
            CurrentStoryLoop = diff;
            switch (diff)
            {
                case StoryLoop.Hard:
                    // Set difficultly changes
                    InitialLightCollisionThreshold = LightCollisionThresholdHard;
                    break;
                case StoryLoop.Easy:
                    // Set difficultly changes
                    InitialLightCollisionThreshold = LightCollisionThresholdEasy;
                    break;
            }
        }

        public static void ToggleDifficulty()
        {
            var diffList = Enum.GetValues(typeof(StoryLoop)).OfType<StoryLoop>().ToList();
            var startingIndex = diffList.IndexOf(CurrentStoryLoop);
            var selectedNextDiff = startingIndex + 1 == diffList.Count
                ? diffList.FirstOrDefault()
                : diffList[startingIndex + 1];
            // Update difficulty
            SetDifficultyTo(selectedNextDiff);
            CurrentStoryLoop = selectedNextDiff;
        }

        #endregion

        #region PostProcessing Values

        public const float BloomIntensity = 12f;
        public const float CcSaturation = 50f;
        public const float CcContrast = 10f;
        public const float LensDistortionIntensity = -40f;
        public const float VignetteIntensity = 0.36f;

        public static GameObject LevelGameObject = null;

        #endregion
    }
}