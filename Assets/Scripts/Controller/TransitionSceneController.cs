﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Controller
{
    /// <summary>
    /// show transformation (post processing and speeding up of things) exampled in a level
    /// </summary>
    public class TransitionSceneController : MonoBehaviourSingleton<TransitionSceneController>
    {
        public CanvasGroup BlackOverlay;
        public PostProcessVolume PpVolume;
        public GameObject TextGameobject;
        public Color ColorGradingOverlayColor;

        private Bloom _bloomLayer = null;
        private ColorGrading _colorGradingLayer = null;
        private LensDistortion _lensDistortionLayer = null;
        private Vignette _vignetteLayer = null;
        private AudioSource _audioSource;
        private float _lensDistortionStartingValue = 40f;

        protected override void Start()
        {
            StartCoroutine(StartPresentationAndSwitchScene());
            _audioSource = gameObject.GetComponent<AudioSource>();
            if (!GlobalGameSettings.AudioMuted())
            {
                _audioSource.volume = 0;
                // Fade in audio
                StartCoroutine(_audioSource.FadeAudioSource(GlobalGameSettings.MaxAudioVolume, 2f));
            }

            base.Start();
        }

        private IEnumerator StartPresentationAndSwitchScene()
        {
            // Get vars for post processing
            PpVolume.profile.TryGetSettings(out _bloomLayer);
            PpVolume.profile.TryGetSettings(out _colorGradingLayer);
            PpVolume.profile.TryGetSettings(out _lensDistortionLayer);
            PpVolume.profile.TryGetSettings(out _vignetteLayer);
            _lensDistortionLayer.intensity.value = _lensDistortionStartingValue;
            _colorGradingLayer.colorFilter.value = Color.white;

            yield return new WaitForSeconds(1f);

            var fadingTime = 1f;
            // Fade out black
            StartCoroutine(BlackOverlay.FadeCanvasGroupTo(0f, fadingTime));
            // Start moving camera slowly
            StartCoroutine(Extension.MoveCamera(15f, 16f));

            yield return new WaitForSeconds(fadingTime + 1f);
            // Slowly enable postprocessing and effects
            StartCoroutine(SlowlyFadePostProcessingValues(6f));
            
            yield return new WaitForSeconds(12f);
            // Finally fade in black
            StartCoroutine(BlackOverlay.FadeCanvasGroupTo(1f, fadingTime));
            // Fadeout audio
            StartCoroutine(_audioSource.FadeAudioSource(0f, fadingTime * 2));
            yield return new WaitForSeconds(fadingTime);

            // Show text
            TextGameobject.SetActive(true);

            yield return new WaitForSeconds(4f);

            TextGameobject.SetActive(false);

            // Mark play through as finished
            // Note: Make sure to only increase this if necessary
            if (GlobalGameSettings.CurrentStoryLoop == GlobalGameSettings.StoryLoop.Easy)
            {
                GlobalGameSettings.CompletedRuns = Math.Max(GlobalGameSettings.CompletedRuns, 1);
            }

            // Increased difficulty
            GlobalGameSettings.SetDifficultyTo(GlobalGameSettings.StoryLoop.Hard);

            yield return new WaitForSeconds(0.6f);
            // Switch scene to normal game scene and start all over again with black overlay open
            GameManager.Instance.SceneTransition = true;
            GameManager.Instance.checkLightCollision = true;

            GameManager.Instance.LoadScene(1);
        }

        private IEnumerator SlowlyFadePostProcessingValues(float time)
        {
            var i = 0.0f;
            var rate = 1.0f / time;
            while (i < 1.0)
            {
                i += Time.deltaTime * rate;
                // Bloom - intensity from 0 to 12
                _bloomLayer.intensity.value = Mathf.Lerp(0, GlobalGameSettings.BloomIntensity, i);
                // fade form current color to target color
                _colorGradingLayer.colorFilter.value = Color.Lerp(_colorGradingLayer.colorFilter.value, ColorGradingOverlayColor, i);
                // Cc - contrast from 0 to 10
                _colorGradingLayer.contrast.value = Mathf.Lerp(0, GlobalGameSettings.CcContrast, i);
                // Cc - saturation from 0 to 50
                _colorGradingLayer.saturation.value = Mathf.Lerp(0, GlobalGameSettings.CcSaturation, i);
                // Lens distortion - intensity from 0 to 40
                _lensDistortionLayer.intensity.value = Mathf.Lerp(_lensDistortionStartingValue,
                    GlobalGameSettings.LensDistortionIntensity, i);
                // Vignetting - intesity from 0 - 0.36
                _vignetteLayer.intensity.value = Mathf.Lerp(0, GlobalGameSettings.VignetteIntensity, i);
                yield return null;
            }

            yield return new WaitForSeconds(time);
        }
    }
}