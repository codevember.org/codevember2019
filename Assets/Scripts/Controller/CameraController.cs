﻿using System;
using System.Collections;
using System.Linq;
using CodevemberShader;
using UnityEngine;

namespace Controller
{
    public class CameraController : MonoBehaviour
    {
        public GameObject player; // TODO: Should this one stay public or fetch automatically?

        private Camera _camera;
        private Animator _animator;
        private GameManager _gameManager;
        private ChromaticAberration _chromaticAberration;

        public bool IsPanning => _isPanning;

        private bool _isPanning;
        private CameraMode _currentCameraMode = CameraMode.Locked;

        private readonly string _animNamePanEditMode = "CameraPanToTop";
        private readonly string _animNamePanPlayMode = "CameraPanToSide";
        private readonly float _cameraFollowSpeed = 3f;

        private enum CameraMode
        {
            Locked,
            FollowPlayer
        }

        private void Start()
        {
            _gameManager = GameManager.Instance;
            _camera = GetComponent<Camera>();
            _animator = gameObject.GetComponent<Animator>();
            _chromaticAberration = GetComponent<ChromaticAberration>();
        }

        private void Update()
        {
            if (_currentCameraMode != CameraMode.FollowPlayer) return;

            if (!_camera)
            {
                Debug.LogError("Camera object is null");
            }
            else if (!player)
            {
                Debug.LogError("Player object is null");
            }
            else
            {
                var position = transform.position;
                var destination = new Vector3(player.transform.position.x, position.y, position.z);
                position = Vector3.MoveTowards(position, destination, _cameraFollowSpeed * Time.deltaTime);
                transform.position = position;
            }
        }

        // Note: Currently unused, reintroduce, when we add the edit mode
        private IEnumerator PanCameraToEditMode()
        {
            _isPanning = true;

            // Start animation
            var clipLength = _animator.runtimeAnimatorController.animationClips
                .First(x => x.name.Equals(_animNamePanEditMode)).length;
            _animator.Play(_animNamePanEditMode);
            yield return new WaitForSeconds(clipLength);
            _isPanning = false;

            StartCoroutine(PanCameraToPlayMode());
        }

        private IEnumerator PanCameraToPlayMode()
        {
            _isPanning = true;

            // Start animation
            var clipLength = _animator.runtimeAnimatorController.animationClips
                .First(x => x.name.Equals(_animNamePanPlayMode)).length;
            _animator.Play(_animNamePanPlayMode);
            yield return new WaitForSeconds(clipLength);
            _isPanning = false;
        }

        // Interface: 

        /** Sets the camera to follow mode */
        public void FollowPlayer()
        {
            if (_currentCameraMode == CameraMode.FollowPlayer) return;

            _currentCameraMode = CameraMode.FollowPlayer;
            Debug.Log("CameraController: Follow Player");
        }

        /** Locks the camera at the current position and exits follow mode */
        public void LockCamera()
        {
            _currentCameraMode = CameraMode.Locked;
            Debug.Log("CameraController: Lock Camera");
        }

        /** Resets the camera to the default position */
        public void ResetCameraToDefault()
        {
            // TODO: Reset to initial position
            LockCamera();
        }
    }
}