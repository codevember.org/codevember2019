﻿using UnityEngine;

namespace Controller
{
    public class LevelController : MonoBehaviour
    {
        public string forcedLevelNumber = null;

        public void Start()
        {
            // Only load specific levels if it's a debug build
            if (Debug.isDebugBuild && !string.IsNullOrEmpty(forcedLevelNumber))
            {
                Debug.Log("Forced loading level no." + forcedLevelNumber);

                // Update difficulty if needed
                GlobalGameSettings.SetDifficultyTo(forcedLevelNumber.StartsWith("1")
                    ? GlobalGameSettings.StoryLoop.Hard
                    : GlobalGameSettings.StoryLoop.Easy);

                GameManager.Instance.LoadPrefabIntoScene(forcedLevelNumber + "_Level");
                FindObjectOfType<PostProcessingCameraChecker>().CheckIfPostProcessingIsNeeded();
            }
            else
            {
                GameManager.Instance.LoadPrefabIntoScene();
            }
        }
    }
}