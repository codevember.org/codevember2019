﻿using UnityEngine;
 
 public class Debug_CameraMovement : MonoBehaviour
 {
     public float speed = 50;

     private void Update()
     {
         float xAxisValue = Input.GetAxis("Horizontal") * speed;
         float zAxisValue = Input.GetAxis("Vertical") * speed;
 
         transform.position = new Vector3(transform.position.x + xAxisValue, transform.position.y, transform.position.z + zAxisValue);
     }
 }