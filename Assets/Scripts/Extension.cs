﻿using System.Collections;
using UnityEngine;

public static class Extension
{
    /// <summary>
    /// Fades the image to aValue in aTime
    /// </summary>
    /// <param name="image"></param>
    /// <param name="aValue"></param>
    /// <param name="aTime"></param>
    /// <returns></returns>
    public static IEnumerator FadeCanvasGroupTo(this CanvasGroup cg, float aValue, float aTime)
    {
        float alpha = cg.alpha;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            cg.alpha = Mathf.Lerp(alpha, aValue, t);
            yield return null;
        }
    }

    /// <summary>
    /// Rounds Vector3.
    /// </summary>
    /// <param name="vector3"></param>
    /// <param name="decimalPlaces"></param>
    /// <returns></returns>
    public static Vector3 Round(this Vector3 vector3, int decimalPlaces = 2)
    {
        float multiplier = 1;
        for (var i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }
        return new Vector3(
            Mathf.Round(vector3.x * multiplier) / multiplier,
            Mathf.Round(vector3.y * multiplier) / multiplier,
            Mathf.Round(vector3.z * multiplier) / multiplier);
    }

    public static IEnumerator FadeAudioSource(this AudioSource audioSource, float targetValue, float time)
    {
        var t = 0f;
        var startValue = audioSource.volume;
        while (t < 1)
        {
            t += Time.deltaTime / time;
            audioSource.volume = Mathf.Lerp(startValue, targetValue, t);
            yield return null;
        }
    }

    public static IEnumerator MoveCamera(float time, float distanceX)
    {
        var i = 0.0f;
        var rate = 1.0f / time;
        var cameraMainTransform = Camera.main.transform;
        var cameraStartPos = cameraMainTransform.position + new Vector3(2f, 0, 0);
        while (i < 1.0)
        {
            i += Time.deltaTime * rate;
            // Lerp between pos
            cameraMainTransform.transform.position = Vector3.Lerp(cameraStartPos,
                cameraStartPos + new Vector3(distanceX, 0, 0), i);
            yield return null;
        }
    }
}