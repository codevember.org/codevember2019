﻿using UnityEngine;

public class SetInitialSettings : MonoBehaviour
{
    public float ScalingFactor = 0.7f;
    
    // Start is called before the first frame update
    void Awake()
    {
        var initialWidth = PlayerPrefs.GetInt("InitialScreenResolutionWidth", -1);
        var initialHeight = PlayerPrefs.GetInt("InitialScreenResolutionHeight", -1);

        if (initialWidth == -1 || initialHeight == -1)
        {
            var currentResolution = Screen.currentResolution;
            initialWidth = currentResolution.width;
            initialHeight = currentResolution.height;
            PlayerPrefs.SetInt("InitialScreenResolutionWidth", initialWidth);
            PlayerPrefs.SetInt("InitialScreenResolutionHeight", initialHeight);
        }

        Screen.SetResolution((int) (initialWidth * ScalingFactor), (int) (initialHeight * ScalingFactor), true);
        Debug.Log("Updated resolution from " + initialWidth + " x " + initialWidth + " to " + Screen.currentResolution);
    }
}
