﻿using UnityEngine;

[CreateAssetMenu(fileName = "ChapterMusicObject", menuName = "ScriptableObjects/ChapterMusicObject", order = 1)]
public class ChapterMusicObject : ScriptableObject
{
    public string clipName;
    public AudioClip audioClip;
}