﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Controller;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    private AudioSource _audioSource;
    // For music only, set max vol = 60% von GlobalSettings.Maxvol
    private float _maxVolMultiplier = 0.6f;

    private float CurrentVol
    {
        get => _audioSource.volume;
        set => _audioSource.volume = value;
    }

    public List<ChapterMusicObject> musicObjects = new List<ChapterMusicObject>();

    public void Start()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
        PlayChapterMusic(-1);
    }

    public void MuteAudioSource(bool b)
    {
        _audioSource.mute = b;
    }

    public void PlayChapterMusic(int chapterNr)
    {
        switch (chapterNr)
        {
            //credits scene
            case -2:
                _audioSource.Stop();
                CurrentVol = 0f;
                _audioSource.pitch = 0.8f;
                SetAudioClip("Credits");
                _audioSource.Play();
                FadeIn(2f);
                break;
            // Main menu
            case -1:
                _audioSource.Stop();
                CurrentVol = 0f;
                _audioSource.pitch = 1f;
                SetAudioClip("MainMenu");
                _audioSource.Play();
                FadeIn(2f);
                break;
            // Chapters
            case 1:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter01");
                _audioSource.Play();
                break;
            case 2:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter02");
                _audioSource.Play();
                break;
            case 3:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter03");
                _audioSource.Play();
                break;
            case 4:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter04");
                _audioSource.Play();
                break;
            case 5:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter05");
                _audioSource.Play();
                break;
            case 6:
                _audioSource.Stop();
                _audioSource.pitch = 1f;
                SetAudioClip("Chapter06");
                _audioSource.Play();
                break;
            default:
                break;
        }

        // SetNextAudioClip();
        if (chapterNr > 0)
        {
            CurrentVol = 0;
            FadeIn(1f);
        }
    }

    private void SetNextAudioClip()
    {
        var currentObj = (from x in musicObjects where x.audioClip == _audioSource.clip select x).FirstOrDefault();
        var currentIndex = musicObjects.IndexOf(currentObj);
        if (musicObjects.Count >= currentIndex + 1) SetAudioClip(musicObjects[currentIndex + 1].clipName);
    }

    private void SetAudioClip(string clipName)
    {
        // Find clip
        var clip = (from x in musicObjects where x.clipName == clipName select x.audioClip).FirstOrDefault();
        if (clip == null)
        {
            Debug.LogError("audioClip with name " + clipName + " not found");
            clip = musicObjects.First().audioClip;
        }

        _audioSource.clip = clip;
    }

    private void FadeIn(float time)
    {
        StopCoroutine("Fade");
        StartCoroutine(_audioSource.FadeAudioSource(GlobalGameSettings.MaxAudioVolume* _maxVolMultiplier, time));
        //StartCoroutine(Fade(CurrentVol, GlobalGameSettings.MaxAudioVolume, time));
    }

    public void FadeOut(float time)
    {
        StopCoroutine("Fade");
        StartCoroutine(_audioSource.FadeAudioSource(0f, time));
        //StartCoroutine(Fade(CurrentVol, 0f, time));
    }

    //private IEnumerator Fade(float from, float to, float time)
    //{
    //    var t = 0f;
    //    while (t < 1)
    //    {
    //        t += Time.deltaTime / time;
    //        CurrentVol = Mathf.Lerp(from, to, t);
    //        yield return null;
    //    }
    //}
}