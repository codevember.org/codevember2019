﻿using UnityEngine;

// Base idea from: https://forum.unity.com/threads/rgb-split-shader.220068/
namespace CodevemberShader
{
    [ExecuteInEditMode]
    public class ChromaticAberration : MonoBehaviour
    {
        #region Variables

        public Shader chromaticAberrationShader;
        public float chromaticAberration = 1.0f;
        private Material _curMaterial;

        #endregion

        #region Properties

        Material Material
        {
            get
            {
                if (_curMaterial == null)
                {
                    _curMaterial = new Material(chromaticAberrationShader);
                    _curMaterial.hideFlags = HideFlags.HideAndDontSave;
                }

                return _curMaterial;
            }
        }

        #endregion


        private void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
        {
            if (chromaticAberrationShader != null)
            {
                Material.SetFloat("_AberrationOffset", chromaticAberration);
                Graphics.Blit(sourceTexture, destTexture, Material);
            }
            else
            {
                Graphics.Blit(sourceTexture, destTexture);
            }
        }

        private void OnDisable()
        {
            if (_curMaterial)
            {
                DestroyImmediate(_curMaterial);
            }
        }
    }
}