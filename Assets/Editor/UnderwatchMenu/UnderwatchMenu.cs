﻿using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

public class UnderwatchMenu : MonoBehaviourSingleton<UnderwatchMenu>
{
    private const string UnderwatchMenuName = "Underwatch";

    [MenuItem(UnderwatchMenuName + "/Take Screenshot")]
    static void Screenshot()
    {
        Instance.StartCoroutine(ScreenShotCo());
    }

    private static IEnumerator ScreenShotCo(string fileName = "Screenshot")
    {
        //wait for draw
        yield return new WaitForEndOfFrame();
        //yield return new WaitForEndOfFrame();
        //yield return null;

        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        //Encode to PNG
        byte[] screenshot = tex.EncodeToPNG();
        Directory.CreateDirectory("Screenshots");
        System.IO.File.WriteAllBytes("Screenshots/" + GameObject.FindGameObjectWithTag("Level").gameObject.name + ".png", screenshot); //fileName + Time.frameCount
    }
}